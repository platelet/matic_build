// /*global window,location*/
// (function (window) {
//   'use strict';
//
//   var Simditor = window.Simditor;
//   var directives = angular.module('simditor',[]);
//
//   directives.directive('simditor', function () {
//
//     var TOOLBAR_DEFAULT = ['title', 'bold', 'italic', 'underline', 'strikethrough', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|', 'link', 'image', 'hr', '|', 'indent', 'outdent'];
//
//     return {
//       require: "?^ngModel",
//       scope: {
//         update: '@'
//       },
//       link: function (scope, element, attrs, ngModel) {
//         console.log(scope);
//         element.append("<div style='height:300px;'></div>");
//
//         var toolbar = scope.$eval(attrs.toolbar) || TOOLBAR_DEFAULT;
//         scope.simditor = new Simditor({
//           textarea: element.children()[0],
//           pasteImage: true,
//           toolbar: toolbar,
//           defaultImage: 'assets/images/image.png',
//           upload: location.search === '?upload' ? {
//               url: '/upload'
//           } : false
//         });
//
//         var $target = element.find('.simditor-body');
//
//         function readViewText() {
//
//             ngModel.$setViewValue($('.simditor-body').html());
//
//             if (attrs.ngRequired != "false") {
//
//                 var text = $('.simditor-body').text();
//
//                 if(text.trim() === "") {
//                     ngModel.$setValidity("required", false);
//                 } else {
//                     ngModel.$setValidity("required", true);
//                 }
//             }
//             ngModel.$commitViewValue();
//
//         }
//
//         ngModel.$render = function () {
//           scope.simditor.focus();
//           $('.simditor-body').html(ngModel.$viewValue);
//         };
//
//         scope.simditor.on('valuechanged', function () {
//           scope.$apply(readViewText);
//           scope.update({value: ngModel.$modelValue});
//           console.log(ngModel);
//         });
//       }
//     };
//   });
// }(window));
