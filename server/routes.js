/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _componentsErrors = require('./components/errors');

var _componentsErrors2 = _interopRequireDefault(_componentsErrors);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _mail = require('./mail');

var _configEnvironment = require('./config/environment');

var _configEnvironment2 = _interopRequireDefault(_configEnvironment);

var _componentsTasksJs = require('./components/tasks.js');

var _componentsCronJs = require('./components/cron.js');

exports['default'] = function (app, passport) {
  /*** API ENDPOINTS ***/
  app.route('/tasks/fixAssessmentResults').get(_componentsTasksJs.fixAssessmentResults);
  app.route('/tasks/setWorkingLevels').get(_componentsTasksJs.assignWorkingLevels);
  app.route('/tasks/createClassrooms').get(_componentsTasksJs.createClassrooms);
  app.route('/tasks/importSchools').get(_componentsTasksJs.importSchools);
  app.route('/cron/closeAssignments').get(_componentsCronJs.closeClassroomAssignments);

  _configEnvironment2['default'].db_api.forEach(function (endpointConfig) {
    if (endpointConfig.length === 2) {
      app.use('/api/' + String(endpointConfig[0]).trim(), require('./api/' + String(endpointConfig[1]).trim())(passport));
    }
  });
  app.use('/api/purchase', require('./api/purchase')(passport));

  app.route('/logout').get(function (req, res) {
    req.logout();
    res.redirect('/');
  });
  //app.use('/pdf', require('./pdf')(passport));
  app.route('/download/:pdf').get(function (req, res) {
    res.download(__dirname + '/pdf/pdfs/' + req.params.pdf);
  });

  app.route('/emails/send').post(function (req, res) {
    var emails = req.body.recipients.map(function (e, i) {
      return e.email;
    });
    (0, _mail.sendEmail)(emails, req.body.subject, req.body.body, function (isSuccess) {
      if (isSuccess) res.send(true);else res.send(false);
    });
  });

  app.route('/.well-known/acme-challenge/:challengeHash').get(function (req, res) {
    var hash = req.params.challengeHash + '.tvZoTWuQgfiNyjL60B8YO3MBL4hJ0Kt6-rCTO_Pi5AQ';
    res.send(hash);
  });

  app.route('/:url(api|auth|components|app|bower_components|assets)/*').get(_componentsErrors2['default'][404]);

  app.route('/*').get(function (req, res) {
    //if (req.headers['x-forwarded-proto'] == 'http') {
    //  res.redirect('https://' + req.headers.host + req.path);
    //} else {
    res.sendFile(_path2['default'].resolve(app.get('appPath') + '/index.html'));
    //}
  });
};

module.exports = exports['default'];
//# sourceMappingURL=routes.js.map
