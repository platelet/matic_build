/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _environment = require('./environment');

var _environment2 = _interopRequireDefault(_environment);

var _socketioFileUpload = require('socketio-file-upload');

var _socketioFileUpload2 = _interopRequireDefault(_socketioFileUpload);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

// When the user disconnects.. perform this
function onDisconnect(socket) {}

// When the user connects.. perform this
function onConnect(socket) {
  // When the client emits 'info', this listens and executes
  socket.on('info', function (data) {
    socket.log(JSON.stringify(data, null, 2));
  });

  // Insert sockets below
  // require('../api/topicAccess/topicAccess.socket').register(socket);
  // require('../api/surveyResponse/surveyResponse.socket').register(socket);
  // require('../api/topicLevelLearningObjectives/topicLevelLearningObjectives.socket').register(socket);
  // require('../api/survey/survey.socket').register(socket);
  // require('../api/feedback/feedback.socket').register(socket);
  // require('../api/studentImprovements/studentImprovements.socket').register(socket);
  // require('../api/subject/subject.socket').register(socket);
  // require('../api/questionMetric/questionMetric.socket').register(socket);
  // require('../api/testResult/testResult.socket').register(socket);
  // require('../api/institution/institution.socket').register(socket);
  // require('../api/userType/userType.socket').register(socket);
  // require('../api/image/image.socket').register(socket);
  // require('../api/question/question.socket').register(socket);
  // require('../api/userProfile/userProfile.socket').register(socket);
  // require('../api/topic/topic.socket').register(socket);
  // require('../api/user/user.socket').register(socket);
}

exports['default'] = function (socketio) {
  // socket.io (v1.x.x) is powered by debug.
  // In order to see all the debug output, set DEBUG (in server/config/local.env.js) to including the desired scope.
  //
  // ex: DEBUG: "http*,socket.io:socket"

  // We can authenticate socket.io users and access their token through socket.decoded_token
  //
  // 1. You will need to send the token in `client/components/socket/socket.service.js`
  //
  // 2. Require authentication here:
  // socketio.use(require('socketio-jwt').authorize({
  //   secret: config.secrets.session,
  //   handshake: true
  // }));

  socketio.on('connection', function (socket) {

    var uploader = new _socketioFileUpload2['default']();
    uploader.dir = _path2['default'].join(__dirname, "../tmp_files/");
    uploader.on("saved", function (event) {
      event.file.clientDetail.base = event.file.base;
      event.file.clientDetail.path = event.file.pathName;
    });
    uploader.listen(socket);

    socket.address = socket.request.connection.remoteAddress + ':' + socket.request.connection.remotePort;

    socket.connectedAt = new Date();

    socket.log = function () {
      for (var _len = arguments.length, data = Array(_len), _key = 0; _key < _len; _key++) {
        data[_key] = arguments[_key];
      }

      console.log.apply(console, ['SocketIO ' + socket.nsp.name + ' [' + socket.address + ']'].concat(data));
    };

    // Call onDisconnect.
    socket.on('disconnect', function () {
      onDisconnect(socket);
      socket.log('DISCONNECTED');
    });

    // Call onConnect.
    onConnect(socket);
    socket.log('CONNECTED');
  });
};

module.exports = exports['default'];
//# sourceMappingURL=socketio.js.map
