/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

// Production specific configuration
// =================================
module.exports = {
  // Server IP
  ip: process.env.OPENSHIFT_NODEJS_IP || process.env.IP || undefined,

  // Server port
  port: process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080,

  // MongoDB connection options
  mongo: {
    //uri: 'mongodb://admin:maticadmintest@matic-shard-00-00-bygio.mongodb.net:27017,matic-shard-00-01-bygio.mongodb.net:27017,matic-shard-00-02-bygio.mongodb.net:27017/matic?ssl=true&replicaSet=Matic-shard-0&authSource=admin&retryWrites=true&w=majority',
    uri: 'mongodb://' + process.env.MONGODB_USER + ':' + process.env.MONGODB_PASSWORD + '@mongodb/' + process.env.MONGODB_DATABASE || 'mongodb://localhost/matic',
    options: {
      useMongoClient: true
    }
  }
};
//# sourceMappingURL=production.js.map
