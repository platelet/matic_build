/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

exports = module.exports = {
  db_api: [['activities', 'activity'], ['assessments', 'assessment'], ['assessmentQuestions', 'assessmentQuestion'], ['awards', 'award'], ['classrooms', 'classroom'], ['classroomAssignments', 'classroomAssignment'], ['characterSets', 'characterSet'], ['dashboardMessages', 'dashboardMessage'], ['feedbacks', 'feedback'], ['files', 'file'], ['games', 'game'], ['gameLevels', 'gameLevel'], ['images', 'image'], ['institutions', 'institution'], ['notifications', 'notification'], ['objectives', 'objective'], ['questions', 'question'], ['questionMetrics', 'questionMetric'], ['schoolInformation', 'schoolInformation'], ['studentGroups', 'studentGroup'], ['studentAssessmentResults', 'studentAssessmentResult'], ['studentProgress', 'studentProgress'], ['studentResults', 'studentResult'], ['subjects', 'subject'], ['surveys', 'survey'], ['surveyresponses', 'surveyResponse'], ['topics', 'topic'], ['users', 'user'], ['userTypes', 'userType'], ['waitlists', 'waitlist'], ['wordProblemCategorys', 'wordProblemCategory'], ['wordProblemSets', 'wordProblemSet'], ['workingLevels', 'workingLevel']]
};
//# sourceMappingURL=endpoints.js.map
