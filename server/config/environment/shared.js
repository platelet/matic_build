/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

exports = module.exports = {
  userRoles: ['guest', 'user', 'admin'],

  colors: ['yellow', 'orange', 'pink', 'violet', 'dblue', 'lblue', 'aqua', 'green'],
  appIcons: ['plus_minus', 'multiply_divide', 'fraction', 'place_value', 'decimal', 'geometry', 'statistics', 'weight', 'measurement', 'question_mark', 'bricks123', 'pulley', 'find', 'order', 'count', 'memory', 'timer', 'survey_reload', 'survey_compass', 'survey_pointer', 'survey_cycle', 'survey_upwardarrow', 'survey_threecircleArrow', 'survey_speechbubble', 'survey_forward'],
  gameTypes: ['bricks', 'number_grid', 'balance', 'order', 'falling_touch', 'falling_connect', 'dice', 'puzzle', 'odd_one_out', 'estimate_circle', 'estimate_line'],
  surveyQuestionTypes: ['text', 'number', 'option', 'select', 'yesno', 'scale', 'slider'],
  surveyScaleTypes: ['full', 'numerical', 'custom', 'smiley'],
  surveySmileyTypes: ['All Smile', 'All Sad', 'Range - Sad to Happy', 'Range - Happy to Sad'],
  sliderTypes: ['Smiley', 'Numerical'],
  characterImgCodes: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
};
//# sourceMappingURL=shared.js.map
