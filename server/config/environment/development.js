/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/matic-full',
    //uri: 'mongodb://admin:maticadmintest@matic-shard-00-00-bygio.mongodb.net:27017,matic-shard-00-01-bygio.mongodb.net:27017,matic-shard-00-02-bygio.mongodb.net:27017/matic?ssl=true&replicaSet=Matic-shard-0&authSource=admin&retryWrites=true&w=majority',
    options: {
      useMongoClient: true
    }
  },

  // Seed database on startup
  seedDB: true

};
//# sourceMappingURL=development.js.map
