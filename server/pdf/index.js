/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _connectEnsureLogin = require('connect-ensure-login');

var _connectEnsureLogin2 = _interopRequireDefault(_connectEnsureLogin);

var express = require('express');
var controller = require('./controller');

var router = express.Router();

var ensureLoggedIn = _connectEnsureLogin2['default'].ensureLoggedIn;

exports['default'] = function (passport) {

  router.post('/renderLogins', controller.generateClassPrintables);
  router.get('/classroomPrintables/:id', controller.renderClassPrintables);
  router.post('/render', controller.render);
  router.get('/a4render', controller.renderA4);
  router.get('/bookletrender', controller.renderBooklet);
  return router;
};

module.exports = exports['default'];
//# sourceMappingURL=index.js.map
