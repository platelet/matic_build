/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.renderClassPrintables = renderClassPrintables;
exports.renderBooklet = renderBooklet;
exports.renderA4 = renderA4;
exports.generateClassPrintables = generateClassPrintables;
exports.render = render;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _apiWordProblemSetWordProblemSetModel = require('../api/wordProblemSet/wordProblemSet.model');

var _apiWordProblemSetWordProblemSetModel2 = _interopRequireDefault(_apiWordProblemSetWordProblemSetModel);

var _apiClassroomClassroomModel = require('../api/classroom/classroom.model');

var _apiClassroomClassroomModel2 = _interopRequireDefault(_apiClassroomClassroomModel);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _configEnvironment = require('../config/environment');

var _configEnvironment2 = _interopRequireDefault(_configEnvironment);

var _phantom = require('phantom');

var _phantom2 = _interopRequireDefault(_phantom);

var path = require('path');

var handlebars = require('handlebars');

//TRANSPORT HELPERS
function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

function fetchProblems(res, id, _cb) {
  _apiWordProblemSetWordProblemSetModel2['default'].find({ _id: id }).populate('problems', 'image').execAsync().then(function (problemSet) {
    var problems = [];
    problems = problemSet[0].problems.map(function (e, i) {
      e.problemStrings = e.problemString.split(/\\r\\n/);
      return e;
    });
    _cb(problems);
  })['catch'](handleError(res));
}

//RENDER SUPPLY
function renderHTML(res, template) {
  return function (problems) {
    _fs2['default'].readFile('server/pdf/view/' + template, 'utf8', function (err, data) {
      if (!err) {
        var _template = handlebars.compile(data);
        var html = _template({ problems: problems });
        res.send(html);
      }
    });
  };
}

function renderPrintables(res, data) {
  console.log('ATTEMPT TO RENDER');
  _fs2['default'].readFile('server/pdf/view/students.handlebars', 'utf8', function (err, templateRead) {
    if (!err) {
      var _template = handlebars.compile(templateRead);
      var html = _template(data);
      console.log('SENDING');
      res.send(html);
    } else {
      console.log(err);
    }
  });
}

function renderClassPrintables(req, res) {
  if (req.params.id) {
    _apiClassroomClassroomModel2['default'].find({ teacher: req.params.id }).populate('students teacher').execAsync().then(function (classroom) {
      if (classroom[0]) {
        var renderData = {
          teacherName: classroom[0].teacher.fullname,
          students: classroom[0].students
        };
        renderPrintables(res, renderData);
      } else {}
    });
  } else {
    res.send('ERROR');
  }
}

//RENDER ROUTE HELPERS

function renderBooklet(req, res) {
  var id = req.query.id;
  fetchProblems(res, id, renderHTML(res, 'booklet.handlebars'));
}

function renderA4(req, res) {
  var id = req.query.id;
  fetchProblems(res, id, renderHTML(res, 'a4.handlebars'));
}

function generatePDF(link, name, type, _cb) {
  var phInstance = undefined;
  var sitepage = undefined;
  var paperSize = {
    width: '600px',
    height: '960px',
    margin: {
      top: '0px',
      left: '0px',
      bottom: '0px',
      right: '0px'
    }
  };
  if (type === 'BOOKLET') {
    paperSize.width = '960px';
    paperSize.height = '600px';
  }
  _phantom2['default'].create([], {
    //phantomPath: process.env.HOME+'/app-root/data/phantomjs/bin/phantomjs'
  }).then(function (instance) {
    phInstance = instance;
    return instance.createPage();
  }).then(function (page) {
    sitepage = page;
    return sitepage.open(link);
  }).then(function (status) {
    if (status === 'success') {
      return sitepage.property('paperSize', paperSize);
    } else {
      sitepage.close();
      phInstance.exit();
      _cb('error');
    }
  }).then(function (status) {
    return sitepage.render(__dirname + '/pdfs/' + name + '.pdf');
  }).then(function (status) {
    sitepage.close();
    phInstance.exit();
    if (status) {
      _cb(name + '.pdf');
    } else {
      _cb('error');
    }
  })['catch'](function (error) {
    sitepage.close();
    phInstance.exit();
    _cb('error');
  });
}

function generateClassPrintables(req, res) {
  var exportType = 'A4';
  var id = req.body.id;
  var link = undefined;

  if (_configEnvironment2['default'].env === 'development') {
    link = 'http://localhost:9000/pdf/classroomPrintables/' + req.user._id;
  } else {
    link = 'https://www.matic.nz/pdf/classroomPrintables' + req.user._id;
  }

  generatePDF(link, req.user._id, exportType, function (file) {
    if (file !== 'error') {
      var base = undefined;
      if (_configEnvironment2['default'].env === 'development') {
        base = 'http://localhost:9000/download/';
      } else {
        base = 'https://www.matic.nz/download/';
      }
      res.send({ url: base + file });
    } else {
      res.status(500).send('error rendering pdf file');
    }
  });
}

function render(req, res) {
  var exportType = req.body.type || 'A4';
  var id = req.body.id;
  var link = undefined,
      base = undefined;

  if (_configEnvironment2['default'].env === 'development') {
    base = 'http://localhost:9000/pdf/';
  } else {
    base = 'https://www.matic.nz/pdf/';
  }

  if (exportType === 'BOOKLET') {
    link = base + 'bookletrender?id=' + id;
  } else {
    link = base + 'a4render?id=' + id;
  }

  generatePDF(link, req.user._id, exportType, function (file) {
    if (file !== 'error') {
      var _base = undefined;
      if (_configEnvironment2['default'].env === 'development') {
        _base = 'http://localhost:9000/download/';
      } else {
        _base = 'https://www.matic.nz/download/';
      }
      res.send({ url: _base + file });
    } else {
      res.status(500).send('error rendering pdf file');
    }
  });
}
//# sourceMappingURL=controller.js.map
