/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _configEnvironment = require('./config/environment');

var _configEnvironment2 = _interopRequireDefault(_configEnvironment);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _passportLocal = require('passport-local');

var _passportLocal2 = _interopRequireDefault(_passportLocal);

var _passportSocketio = require('passport.socketio');

var _passportSocketio2 = _interopRequireDefault(_passportSocketio);

var _apiUserUserModelJs = require('./api/user/user.model.js');

var _apiUserUserModelJs2 = _interopRequireDefault(_apiUserUserModelJs);

var _socketioFileUpload = require('socketio-file-upload');

var _socketioFileUpload2 = _interopRequireDefault(_socketioFileUpload);

_mongoose2['default'].Promise = require('bluebird');

var LocalStrategy = _passportLocal2['default'].Strategy;

// Connect to MongoDB
_mongoose2['default'].connect(_configEnvironment2['default'].mongo.uri, _configEnvironment2['default'].mongo.options);
_mongoose2['default'].connection.on('error', function (err) {
  console.error('MongoDB connection error: ' + err);
  process.exit(-1);
});

// Populate databases with sample data
if (_configEnvironment2['default'].seedDB) {
  require('./config/seed');
}

// Setup server
var app = (0, _express2['default'])();
var server = _http2['default'].createServer(app);
var socketio = require('socket.io')(server, {
  serveClient: true,
  path: '/socket.io-client'
});
app.use(_socketioFileUpload2['default'].router);
require('./config/socketio')(socketio);
require('./config/express')(app);
app.use(_passport2['default'].initialize());
app.use(_passport2['default'].session());
require('./routes')(app, _passport2['default']);

//login middleware declaration -- LOCAL HTTP
_passport2['default'].use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true
}, function (req, username, password, done) {
  var lT = req.body.loginType;
  if (lT === 'student') {
    username = username.toLowerCase();
    password = password.toLowerCase();
    _apiUserUserModelJs2['default'].findOne({ code: password }, function (err, user) {
      if (err) {
        console.log('ERR: not found');return done(err);
      }
      if (!user) {
        console.log('NO USER: not found');return done(null, false);
      }
      if (!user.verifyPassword(username)) {
        console.log('not right');return done(null, false);
      }
      if (!user.dashboardEnabled) {
        console.log('not enabled');return done(null, false);
      }
      return done(null, user);
    });
  } else {
    _apiUserUserModelJs2['default'].findOne({ email: username }, function (err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false);
      }
      if (!user.verifyPassword(password)) {
        return done(null, false);
      }
      //if (!user.dashboardEnabled) {console.log('not enabled'); return done(null, false); }
      return done(null, user);
    });
  }
}));

_passport2['default'].serializeUser(function (user, done) {
  done(null, user._id);
});

_passport2['default'].deserializeUser(function (id, done) {
  _apiUserUserModelJs2['default'].findOne({ _id: id }).select('firstName lastName email info userType stripeCustomer settings dashboardEnabled userWelcomed returnTo createdAt').populate('userType', 'title permissionEnum redirectTo').execAsync().then(function (user) {
    done(null, user);
  })['catch'](done);
});

// //PUT ALL STUDENTS INTO GROUPS
// import UserType from './api/userType/userType.model.js';
// import StudentGroup from './api/studentGroup/studentGroup.model.js';
//
// //GET USER TYPES
// UserType.findOne({title: 'teacher'}, (err, type) => {
//   User.find({userType: type})
//     .execAsync()
//     .then(teachers => {
//       teachers.forEach(teacher => {
//         StudentGroup.find({teacher: teacher})
//           .execAsync()
//           .then(groups => {
//             if (groups.length > 0) {
//               console.log(teacher.fullname, 'has group');
//             } else {
//               console.log(teacher.fullname, 'needs group');
//               User.find({'info.teacher': teacher})
//                 .execAsync()
//                 .then(students => {
//                   let studentIds = students.map((e, i) => {
//                     return e._id;
//                   });
//                   let newGroup = {
//                     teacher: teacher,
//                     students: studentIds,
//                     name: 'New Student Group',
//                     groupIndex: 1,
//                     isDefault: true
//                   };
//                   User.findOneAndUpdate({_id: teacher._id}, {userWelcomed: true}, {upsert:true}, function (err, doc) {
//                     console.log('teacher updated');
//                   });
//                   StudentGroup.createAsync(newGroup)
//                     .then(_newGroup => {
//                       console.log('Group', _newGroup._id, 'created for', teacher.fullname);
//                     });
//                 });
//             }
//           })
//       });
//     });
// });

// Start server
function startServer() {
  try {
    _fs2['default'].mkdirSync(_path2['default'].join(__dirname + '/tmp_files'));
  } catch (err) {}

  app.angularFullstack = server.listen(_configEnvironment2['default'].port, _configEnvironment2['default'].ip, function () {
    console.log('Express server listening on %d, in %s mode', _configEnvironment2['default'].port, app.get('env'));
  });
}

setImmediate(startServer);

// Expose app
exports = module.exports = app;
//# sourceMappingURL=app.js.map
