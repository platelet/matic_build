"use strict";

var _regeneratorRuntime = require("babel-runtime/regenerator")["default"];

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _this = this;

var Stripe = require("stripe")("sk_test_IpLTO1z0nM9Xa69Nu4Q28idP");

exports["default"] = {
  createCustomerForUser: function createCustomerForUser(user, sourceToken) {
    return _regeneratorRuntime.async(function createCustomerForUser$(context$1$0) {
      while (1) switch (context$1$0.prev = context$1$0.next) {
        case 0:
          context$1$0.prev = 0;
          context$1$0.next = 3;
          return _regeneratorRuntime.awrap(Stripe.customers.create({
            email: user.email,
            source: sourceToken,
            metadata: {
              firstName: user.firstName,
              lastName: user.lastName
            }
          }));

        case 3:
          stripeCustomer = context$1$0.sent;

          console.log('new stripe customer', stripeCustomer);
          context$1$0.next = 10;
          break;

        case 7:
          context$1$0.prev = 7;
          context$1$0.t0 = context$1$0["catch"](0);

          console.log('ERROR:', context$1$0.t0);

        case 10:
        case "end":
          return context$1$0.stop();
      }
    }, null, this, [[0, 7]]);
  },

  getCustomerForUser: function getCustomerForUser(user) {
    var stripeCustomer;
    return _regeneratorRuntime.async(function getCustomerForUser$(context$1$0) {
      while (1) switch (context$1$0.prev = context$1$0.next) {
        case 0:
          if (!user.stripeCustomer) {
            context$1$0.next = 13;
            break;
          }

          context$1$0.prev = 1;
          context$1$0.next = 4;
          return _regeneratorRuntime.awrap(Stripe.customers.retrieve(user.stripeCustomer));

        case 4:
          stripeCustomer = context$1$0.sent;

          console.log('got stripe customer', stripeCustomer);
          context$1$0.next = 11;
          break;

        case 8:
          context$1$0.prev = 8;
          context$1$0.t0 = context$1$0["catch"](1);

          console.log(context$1$0.t0);
          // error handling

        case 11:
          context$1$0.next = 14;
          break;

        case 13:
          return context$1$0.abrupt("return", null);

        case 14:
        case "end":
          return context$1$0.stop();
      }
    }, null, this, [[1, 8]]);
  },

  getCustomerPaymentMethods: function getCustomerPaymentMethods(customer) {
    return _regeneratorRuntime.async(function getCustomerPaymentMethods$(context$1$0) {
      while (1) switch (context$1$0.prev = context$1$0.next) {
        case 0:
        case "end":
          return context$1$0.stop();
      }
    }, null, _this);
  },

  addPaymentMethod: function addPaymentMethod(customer, methodToken, isDefault) {
    return _regeneratorRuntime.async(function addPaymentMethod$(context$1$0) {
      while (1) switch (context$1$0.prev = context$1$0.next) {
        case 0:
        case "end":
          return context$1$0.stop();
      }
    }, null, _this);
  }
};
module.exports = exports["default"];
//# sourceMappingURL=index.js.map
