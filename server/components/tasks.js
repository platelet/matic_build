'use strict';

var _Object$keys = require('babel-runtime/core-js/object/keys')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _apiSchoolInformationSchoolInformationModel = require('../api/schoolInformation/schoolInformation.model');

var _apiSchoolInformationSchoolInformationModel2 = _interopRequireDefault(_apiSchoolInformationSchoolInformationModel);

var _apiStudentAssessmentResultStudentAssessmentResultModel = require('../api/studentAssessmentResult/studentAssessmentResult.model');

var _apiStudentAssessmentResultStudentAssessmentResultModel2 = _interopRequireDefault(_apiStudentAssessmentResultStudentAssessmentResultModel);

var _apiClassroomAssignmentClassroomAssignmentModel = require('../api/classroomAssignment/classroomAssignment.model');

var _apiClassroomAssignmentClassroomAssignmentModel2 = _interopRequireDefault(_apiClassroomAssignmentClassroomAssignmentModel);

var _apiStudentGroupStudentGroupModel = require('../api/studentGroup/studentGroup.model');

var _apiStudentGroupStudentGroupModel2 = _interopRequireDefault(_apiStudentGroupStudentGroupModel);

var _apiUserUserModel = require('../api/user/user.model');

var _apiUserUserModel2 = _interopRequireDefault(_apiUserUserModel);

var _apiClassroomClassroomModel = require('../api/classroom/classroom.model');

var _apiClassroomClassroomModel2 = _interopRequireDefault(_apiClassroomClassroomModel);

var _apiWorkingLevelWorkingLevelModel = require('../api/workingLevel/workingLevel.model');

var _apiWorkingLevelWorkingLevelModel2 = _interopRequireDefault(_apiWorkingLevelWorkingLevelModel);

var csv = require("fast-csv");

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    console.log(err);
    res.status(statusCode).send(err);
  };
}

function makeLevelMap(levelArray) {
  var map = {};
  levelArray.forEach(function (level) {
    //check topic level map object
    if (!level.assessment || !level.assessment.topic || !level.student) {
      return;
    }

    if (!map[level.assessment.topic]) {
      map[level.assessment.topic] = {};
    }

    if (map[level.assessment.topic][level.student]) {
      var pl = map[level.assessment.topic][level.student];
      map[level.assessment.topic][level.student] = pl.workingLevel < level.workingLevel ? level : pl;
    } else {
      map[level.assessment.topic][level.student] = level;
    }
  });
  return map;
}

function topicMapToOpArray(topicMap) {
  var bulkWriteOps = [];

  var topicKeys = _Object$keys(topicMap);
  topicKeys.forEach(function (topicId) {
    var topicObj = topicMap[topicId];
    var studentKeys = _Object$keys(topicObj);
    studentKeys.forEach(function (studentId) {
      var studentObj = topicObj[studentId];
      if (studentObj._id) {
        bulkWriteOps.push({ updateOne: {
            filter: { _id: studentObj._id },
            update: { workingLevel: studentObj.workingLevel }
          } }); //update op
      } else {
          bulkWriteOps.push({ insertOne: { document: studentObj } }); //insert op
        }
    });
  });

  return bulkWriteOps;
}

function assignWorkingLevels(req, res) {
  var studentTopicMap = {};
  _apiWorkingLevelWorkingLevelModel2['default'].find({}).then(function (workingLevels) {
    studentTopicMap = makeLevelMap(workingLevels);
    return true;
  }).then(function (response) {
    _apiStudentAssessmentResultStudentAssessmentResultModel2['default'].find({ 'assessmentState.currentState': 'FINISHED' }).populate('assessment').sort('-completedAt').execAsync().then(function (assessmentResults) {
      assessmentResults.forEach(function (result) {
        if (!result.assessment || !result.assessment.topic) return;
        if (!result.student) return;
        var workingLevel = result.result.workingLevel;
        var student = result.student;
        var topic = result.assessment.topic;

        if (studentTopicMap[topic]) {
          if (studentTopicMap[topic][student] && studentTopicMap[topic][student].workingLevel) {
            var pl = studentTopicMap[topic][student];
            studentTopicMap[topic][student].workingLevel = pl.workingLevel < workingLevel ? workingLevel : pl.workingLevel;
          } else {
            studentTopicMap[topic][student] = {
              student: student,
              topic: topic,
              workingLevel: workingLevel,
              assessmentLink: result.assessment._id,
              assignmentMethod: 'ADMIN_MANUAL'
            };
          }
        } else {
          studentTopicMap[topic] = {};
          studentTopicMap[topic][student] = {
            student: student,
            topic: topic,
            workingLevel: workingLevel,
            assessmentLink: result.assessment._id,
            assignmentMethod: 'ADMIN_MANUAL'
          };
        }
      });
      return true;
    }).then(function (response) {
      var ops = topicMapToOpArray(studentTopicMap);
      _apiWorkingLevelWorkingLevelModel2['default'].bulkWrite(ops).then(respondWithResult(res))['catch'](handleError(res));
    });
  })['catch'](handleError(res));
}

function getAssessmentResult(assessmentResult) {
  var score = 0;
  var questionStates = [];
  var workingLevel = 0;

  //question game 1
  if (assessmentResult.linkedResults) {
    var questionGameResult = assessmentResult.linkedResults[0];
    if (questionGameResult && questionGameResult.questionMeta.questions) {
      (function () {
        var questionMeta = questionGameResult.questionMeta.questions;
        _Object$keys(questionMeta).forEach(function (key, index) {
          questionStates[index] = questionMeta[key].correct;
          if (questionMeta[key].correct) score++;
        });
      })();
    }
  }

  //question game 2
  if (assessmentResult.gamePathway && assessmentResult.gamePathway[1] === 'B1') {
    var questionGameResult = assessmentResult.linkedResults[1];
    if (questionGameResult && questionGameResult.questionMeta.questions) {
      (function () {
        var questionMeta = questionGameResult.questionMeta.questions;
        _Object$keys(questionMeta).forEach(function (key, index) {
          questionStates[index + 25] = questionMeta[key].correct;
          if (questionMeta[key].correct) score++;
        });
      })();
    }
  }

  score *= 2;
  workingLevel = getWorkingLevel(questionStates);
  return {
    score: score,
    workingLevel: workingLevel
  };
}

function getWorkingLevel(questionStates) {
  var workingLevel = null;
  for (var level = 0; level < 5; level++) {
    var levelIncorrect = 0;
    for (var objective = 0; objective < 10; objective++) {
      var matrixIndex = level * 10 + objective;
      if (!questionStates[matrixIndex]) levelIncorrect++;
    }
    if (levelIncorrect >= 2 && !workingLevel) {
      workingLevel = level + 1;
    }
  }
  return workingLevel;
}

function fixAssessmentCompletionTime(assessmentResult) {
  var lastResult = assessmentResult.linkedResults[assessmentResult.linkedResults.length - 1];
  var completedAt = lastResult.completedAt;
  assessmentResult.completedAt = completedAt;
  return assessmentResult;
}

function fixAssessmentResults(req, res) {
  _apiClassroomAssignmentClassroomAssignmentModel2['default'].find({ assignmentType: 'ASSESSMENT' }).exec().then(function (assignments) {
    assignments.forEach(function (assignment) {
      console.log('ASSIGNMENT: ', assignment._id);
      var ASSIGNMENT_RESULTS = [];
      _apiStudentAssessmentResultStudentAssessmentResultModel2['default'].find({ assignment: assignment._id }).populate('linkedResults').sort('-completedAt').execAsync().then(function (assessmentResults) {
        assessmentResults.forEach(function (assessmentResult) {
          if (assessmentResult.assessmentState.currentState === 'FINISHED') {
            console.log('ASSESSMENTRESULT: ', assessmentResult._id);
            var hasChanged = false;
            if (!assessmentResult.completedAt) {
              console.log(assessmentResult._id, ' fixing completion time');
              assessmentResult = fixAssessmentCompletionTime(assessmentResult);
              hasChanged = true;
            }
            if (assessmentResult.result && assessmentResult.result['score'] !== undefined && assessmentResult.result['workingLevel'] !== undefined) {
              console.log(assessmentResult._id, ' NO UPDATE NEEDED');
            } else {
              console.log(assessmentResult._id, ' UPDATE NEEDED');
              var calculatedResult = getAssessmentResult(assessmentResult);
              assessmentResult.result = calculatedResult;
              console.log(assessmentResult.result);
              hasChanged = true;
            }
            ASSIGNMENT_RESULTS.push(assessmentResult);
            if (hasChanged) {
              console.log(assessmentResult._id, ' SAVING...');
              assessmentResult.save();
            }
          }
        });
        console.log('ASSIGNMENT SAVE', ASSIGNMENT_RESULTS);
        assignment.linkedAssessmentResults = ASSIGNMENT_RESULTS;
        assignment.save();
      });
    });
    return true;
  }).then(function (confirm) {
    console.log('FINISHED');
    res.status(200).send('COMPLETED');
  })['catch'](handleError(res));
}

function _fixAssessmentResults(req, res) {
  //get all assessments
  var numChanged = 0;
  console.log('--- ASSESSMENT FIX BEGIN ---');
  _apiStudentAssessmentResultStudentAssessmentResultModel2['default'].find({}).populate('linkedResults').sort('-completedAt').batchSize(10000).execAsync().then(function (assessmentResults) {
    console.log(assessmentResults.length, ' RESULTS FOUND');
    assessmentResults.forEach(function (assessmentResult) {
      if (assessmentResult.linkedResults.length >= 2) {
        //assessment should be completed!
        //is it marked as completed?
        var hasChanged = false;
        console.log(assessmentResult._id, ' STARTED');
        if (!assessmentResult.completedAt) {
          console.log(assessmentResult._id, ' fixing completion time');
          assessmentResult = fixAssessmentCompletionTime(assessmentResult);
          hasChanged = true;
        }
        if (!assessmentResult.assessmentState.currentState === 'FINISHED') {
          console.log(assessmentResult._id, ' fixing state');
          assessmentResult.assessmentState.currentState = 'FINISHED';
          hasChanged = true;
        }

        //has the score been calculated?
        if (assessmentResult.result && assessmentResult.result['score'] !== undefined && assessmentResult.result['workingLevel'] !== undefined) {
          console.log(assessmentResult._id, ' NO UPDATE NEEDED');
        } else {
          console.log(assessmentResult._id, ' UPDATE NEEDED');
          console.log(assessmentResult.result);
          var calculatedResult = getAssessmentResult(assessmentResult);
          assessmentResult.result = calculatedResult;
          hasChanged = true;
        }

        if (hasChanged) {
          console.log(assessmentResult._id, ' SAVING...');
          numChanged++;
          assessmentResult.save();
        }
      } else {
        //assessment not yet finished
        console.log(assessmentResult._id, ' not completed');
      }
    });
    return true;
  }).then(function (confirm) {
    console.log(numChanged, ' records changed');
    res.status(200).send('COMPLETED');
  })['catch'](handleError(res));
}

function createClassrooms(req, res) {
  //get the teachers
  _apiUserUserModel2['default'].find().populate('userType').execAsync().then(function (users) {
    var teachers = users.filter(function (user) {
      if (user.userType) {
        return user.userType.title === 'teacher';
      } else {
        return false;
      }
    });
    return teachers;
  }).then(function (teachers) {
    teachers.forEach(function (teacher) {});
    console.log('Working on ', teachers[0].fullname);
    return _apiStudentGroupStudentGroupModel2['default'].find({ teacher: teachers[0]._id }).populate('teacher students').execAsync().then(function (groups) {
      console.log('Found ', groups.length, ' groups');
    });
  }).then(function (confirm) {
    res.status(200).send('COMPLETED');
  })['catch'](handleError(res));
}

function rowCSVToJSON(row) {
  return {
    schoolNumber: row[0],
    schoolName: row[1],
    principal: row[2],
    contact: {
      telephone: row[3],
      fax: row[4],
      email: row[5],
      website: row[6]
    },
    address: {
      street: row[7],
      suburb: row[8],
      townCity: row[9],
      postalAddress_1: row[10],
      postalAddress_2: row[11],
      postalAddress_3: row[12],
      postalCode: row[13],
      urbanArea: row[14],
      latlng: {
        longitude: row[15],
        latitude: row[16]
      }
    },
    schoolType: row[17],
    definition: row[18],
    schoolGender: row[19],
    communityOfLearning: {
      id: row[20],
      name: row[21]
    },
    schoolRoll: {
      total: row[22],
      europeanPakeha: row[23],
      maori: row[24],
      pacific: row[25],
      asian: row[26],
      MELAA: row[27],
      other: row[28],
      international: row[29]
    },
    decile: row[30]
  };
}

function importSchools(req, res) {
  var stream = _fs2['default'].createReadStream(_path2['default'].join(__dirname + '/schools.csv'));
  var schoolData = [];

  var csvStream = csv.parse({ headers: false }).on("data", function (data) {
    schoolData.push(rowCSVToJSON(data));
  }).on("end", function () {
    console.log('DONE WITH PARSE: ', schoolData.length);
    _apiSchoolInformationSchoolInformationModel2['default'].insertMany(schoolData, function (errors, docs) {
      console.log(errors);
      console.log(docs.length);
      res.send('done');
    });
  });

  stream.pipe(csvStream);
}

exports.assignWorkingLevels = assignWorkingLevels;
exports.fixAssessmentResults = fixAssessmentResults;
exports.createClassrooms = createClassrooms;
exports.importSchools = importSchools;
//# sourceMappingURL=tasks.js.map
