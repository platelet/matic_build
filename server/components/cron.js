/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _apiClassroomAssignmentClassroomAssignmentModel = require('../api/classroomAssignment/classroomAssignment.model');

var _apiClassroomAssignmentClassroomAssignmentModel2 = _interopRequireDefault(_apiClassroomAssignmentClassroomAssignmentModel);

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    console.log(err);
    res.status(statusCode).send(err);
  };
}

function closeClassroomAssignments(req, res) {
  var now = Date.now();
  _apiClassroomAssignmentClassroomAssignmentModel2['default'].find({ assignmentType: { $ne: 'TOPIC' }, isActive: true, 'assignmentPeriod.dateEnd': { $lt: now } }).execAsync().then(function (assignments) {
    var deletedCount = 0;
    var closedCount = 0;
    var deletionIds = [];
    assignments.forEach(function (assignment) {
      var resCount = assignment.linkedResults.length + assignment.linkedSurveyResponses.length + assignment.linkedAssessmentResults.length;
      var shouldDelete = resCount === 0;
      if (shouldDelete) {
        deletionIds.push(assignment._id);
        deletedCount++;
      } else {
        assignment.isActive = false;
        assignment.save();
        closedCount++;
      }
    });
    _apiClassroomAssignmentClassroomAssignmentModel2['default'].deleteMany({ _id: { $in: deletionIds } }, function (err) {
      if (err) {
        console.log(err);
      }
    });
    console.log('CLASSROOM ASSIGNMENT TASK COMPLETED', now, 'DELETED:', deletedCount, 'CLOSED:', closedCount);
    return 'Closed: ' + closedCount + '. Deleted: ' + deletedCount;
  }).then(respondWithResult(res))['catch'](handleError(res));
}

exports.closeClassroomAssignments = closeClassroomAssignments;
//# sourceMappingURL=cron.js.map
