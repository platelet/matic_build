'use strict';

exports.ensureAPIUser = function (apiUsers) {
  if (apiUsers = 'any') {
    apiUsers = ['student', 'teacher', 'admin'];
  }
  return function (req, res, next) {
    if ((req.isAuthenticated || req.isAuthenticated()) && req.user && req.user.userType) {
      if (apiUsers.indexOf(req.user.userType.title) !== -1) {
        next();
        return;
      }
    }
    res.status(403).send('PERMISSION DENIED');
  };
};
//# sourceMappingURL=index.js.map
