/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _xoauth2 = require('xoauth2');

var _xoauth22 = _interopRequireDefault(_xoauth2);

var transporter = _nodemailer2['default'].createTransport('smtps://ian@matic.nz:peaceandmaths@smtp.gmail.com');

var hbs = require('nodemailer-express-handlebars');
var handlebars = hbs({
  viewEngine: {
    layoutsDir: "server/mail/layouts/",
    partialsDir: "server/mail/views/",
    defaultLayout: "mainLayout"
  },
  viewPath: "server/mail/views"
});
transporter.use('compile', handlebars);

function sendForgotEmail(_transporter) {
  var _transporter = _transporter;
  return function (to, name, resetCode) {
    var mailOptions = {
      from: '"Matic" <ian@matic.nz>',
      to: to,
      subject: 'Matic | Password Reset',
      template: 'forgot',
      context: {
        name: name,
        resetCode: resetCode
      }
    };
    _transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
        return;
      }
      console.log('Message sent: ' + info.response);
    });
  };
}

function sendVerifyEmail(_transporter) {
  var _transporter = _transporter;
  return function (to, name, code) {
    var mailOptions = {
      from: '"Matic" <ian@matic.nz>',
      to: to,
      subject: 'Matic | Please verify your email',
      template: 'verify',
      context: {
        name: name,
        code: code
      }
    };
    _transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
        return;
      }
      console.log('Message sent: ' + info.response);
    });
  };
}

function sendWaitlistedEmail(_transporter) {
  var _transporter = _transporter;
  return function (to) {
    var mailOptions = {
      from: '"Matic" <ian@matic.nz>',
      to: to,
      subject: 'Your on the Matic waitlist!',
      template: 'waitlisted',
      context: {}
    };
    _transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
        return;
      }
      console.log('Message Sent: ' + info.response);
    });
  };
}

function mailWrapper(_transporter) {
  var _transporter = _transporter;
  return function (to, subject, message, _cb) {
    var mailOptions = {
      from: '"Matic" <ian@matic.nz>',
      bcc: to,
      subject: subject,
      template: 'message',
      context: {
        message: message
      }
    };
    _transporter.sendMail(mailOptions, function (error, info) {
      _cb(!error);
      if (error) {
        console.log(error);
        return;
      }
      console.log('Message sent: ' + info.response);
    });
  };
}

exports.sendForgotEmail = sendForgotEmail(transporter);
exports.sendVerifyEmail = sendVerifyEmail(transporter);
exports.sendEmail = mailWrapper(transporter);
exports.sendWaitlistedEmail = sendWaitlistedEmail(transporter);
//# sourceMappingURL=index.js.map
