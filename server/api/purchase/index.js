/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _componentsEnsureApiUser = require('../../components/ensureApiUser');

var _componentsEnsureApiUser2 = _interopRequireDefault(_componentsEnsureApiUser);

var express = require('express');
var controller = require('./purchase.controller');

var router = express.Router();
var ensureAPIUser = _componentsEnsureApiUser2['default'].ensureAPIUser;

exports['default'] = function (passport) {

  router.get('/sources', ensureAPIUser(['teacher']), controller.getSources);
  router.post('/', ensureAPIUser(['teacher']), controller.purchase);

  return router;
};

module.exports = exports['default'];
//# sourceMappingURL=index.js.map
