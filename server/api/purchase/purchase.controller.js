/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/users              ->  index
 * POST    /api/users              ->  create
 * GET     /api/users/:id          ->  show
 * PUT     /api/users/:id          ->  update
 * DELETE  /api/users/:id          ->  destroy
 */

'use strict';

var _regeneratorRuntime = require('babel-runtime/regenerator')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.getSources = getSources;
exports.purchase = purchase;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _userUserModel = require('../user/user.model');

var _userUserModel2 = _interopRequireDefault(_userUserModel);

var _userTypeUserTypeModel = require('../userType/userType.model');

var _userTypeUserTypeModel2 = _interopRequireDefault(_userTypeUserTypeModel);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Types.ObjectId;
var Stripe = require("stripe")("sk_test_IpLTO1z0nM9Xa69Nu4Q28idP");

var subscriptionPlan = 'matic_full_annual';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      delete entity._hash;
      delete entity._salt;
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

function getSources(req, res) {
  var stripeCustomer;
  return _regeneratorRuntime.async(function getSources$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        if (!(req.user.stripeCustomer.length > 4)) {
          context$1$0.next = 15;
          break;
        }

        stripeCustomer = undefined;
        context$1$0.prev = 2;
        context$1$0.next = 5;
        return _regeneratorRuntime.awrap(Stripe.customers.retrieve(req.user.stripeCustomer));

      case 5:
        stripeCustomer = context$1$0.sent;
        context$1$0.next = 11;
        break;

      case 8:
        context$1$0.prev = 8;
        context$1$0.t0 = context$1$0['catch'](2);

        handleError(res, 500)(context$1$0.t0);

      case 11:

        console.log(stripeCustomer);
        if (stripeCustomer && stripeCustomer.sources && !stripeCustomer.deleted) {
          respondWithResult(res)(stripeCustomer.sources.data);
        } else {
          _userUserModel2['default'].findByIdAsync(req.user._id).then(handleEntityNotFound(res)).then(saveUpdates({
            stripeCustomer: ''
          }));
          respondWithResult(res)([]);
        }

        context$1$0.next = 16;
        break;

      case 15:
        handleError(res, 403)('Permission Denied');

      case 16:
      case 'end':
        return context$1$0.stop();
    }
  }, null, this, [[2, 8]]);
}

function purchase(req, res) {
  var stripeCustomer, paymentObject, purchase;
  return _regeneratorRuntime.async(function purchase$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        stripeCustomer = undefined;

        if (!req.user.stripeCustomer) {
          context$1$0.next = 13;
          break;
        }

        context$1$0.prev = 2;
        context$1$0.next = 5;
        return _regeneratorRuntime.awrap(Stripe.customers.retrieve(req.user.stripeCustomer));

      case 5:
        stripeCustomer = context$1$0.sent;
        context$1$0.next = 11;
        break;

      case 8:
        context$1$0.prev = 8;
        context$1$0.t0 = context$1$0['catch'](2);

        console.log(context$1$0.t0);

      case 11:
        context$1$0.next = 23;
        break;

      case 13:
        context$1$0.prev = 13;
        context$1$0.next = 16;
        return _regeneratorRuntime.awrap(Stripe.customers.create({
          email: req.user.email,
          source: req.body.source.token,
          metadata: {
            firstName: req.user.firstName,
            lastName: req.user.lastName
          }
        }));

      case 16:
        stripeCustomer = context$1$0.sent;

        _userUserModel2['default'].findByIdAsync(req.user._id).then(handleEntityNotFound(res)).then(saveUpdates({
          stripeCustomer: stripeCustomer.id
        })).then(function (user) {
          console.log('saved user', user);
        })['catch'](handleError(res));
        context$1$0.next = 23;
        break;

      case 20:
        context$1$0.prev = 20;
        context$1$0.t1 = context$1$0['catch'](13);

        console.log('ERROR:', context$1$0.t1);

      case 23:
        paymentObject = {
          amount: req.body.price * 100,
          currency: 'nzd',
          description: req.body.itemDescription,
          receipt_email: req.user.email,
          customer: stripeCustomer.id,
          source: req.body.source.card
        };
        context$1$0.next = 26;
        return _regeneratorRuntime.awrap(Stripe.charges.create(paymentObject));

      case 26:
        purchase = context$1$0.sent;

        respondWithResult(res)(purchase);

      case 28:
      case 'end':
        return context$1$0.stop();
    }
  }, null, this, [[2, 8], [13, 20]]);
}
//# sourceMappingURL=purchase.controller.js.map
