/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _componentsEnsureApiUser = require('../../components/ensureApiUser');

var _componentsEnsureApiUser2 = _interopRequireDefault(_componentsEnsureApiUser);

var express = require('express');
var controller = require('./assessmentQuestion.controller');

var router = express.Router();
var ensureAPIUser = _componentsEnsureApiUser2['default'].ensureAPIUser;

exports['default'] = function (passport) {

  router.get('/', ensureAPIUser('any'), controller.index);
  router.get('/:assessment/all', ensureAPIUser(['admin']), controller.showAssessmentQuestions);
  router.get('/:assessment/:test', ensureAPIUser('any'), controller.showTest);
  router.get('/:id', ensureAPIUser('any'), controller.show);
  router.post('/bulkGet', ensureAPIUser('any'), controller.fetchBulk);
  router.post('/', ensureAPIUser(['admin']), controller.create);
  router.post('/bulk', ensureAPIUser(['admin']), controller.bulk);
  router.put('/:id', ensureAPIUser(['admin']), controller.update);
  router.patch('/:id', ensureAPIUser(['admin']), controller.update);
  router['delete']('/:id', ensureAPIUser(['admin']), controller.destroy);
  return router;
};

module.exports = exports['default'];
//# sourceMappingURL=index.js.map
