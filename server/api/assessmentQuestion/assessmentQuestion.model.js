/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var AssessmentQuestionSchema = new mongoose.Schema({
  assessment: { type: ObjectId, ref: 'Assessment' },
  assessmentQuestionGame: { type: String, 'enum': ['A', 'B'] },
  question: {
    english: String,
    maori: String,
    spanish: String
  },
  choices: Array,
  answer: {
    english: String,
    maori: String,
    spanish: String
  },
  questionNumber: Number,
  questionType: { type: String, 'default': 'text' },
  image: { type: ObjectId, ref: 'Image' },
  summary: String,
  description: String,
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

exports['default'] = mongoose.model('AssessmentQuestion', AssessmentQuestionSchema);
module.exports = exports['default'];
//# sourceMappingURL=assessmentQuestion.model.js.map
