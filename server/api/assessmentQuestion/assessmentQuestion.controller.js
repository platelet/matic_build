/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/questions         ->  index
 * POST    /api/questions         ->  create
 * GET     /api/questions/:id     ->  show
 * PUT     /api/questions/:id     ->  update
 * DELETE  /api/questions/:id     ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.fetchBulk = fetchBulk;
exports.show = show;
exports.showAssessmentQuestions = showAssessmentQuestions;
exports.showTest = showTest;
exports.create = create;
exports.bulk = bulk;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _assessmentQuestionModel = require('./assessmentQuestion.model');

var _assessmentQuestionModel2 = _interopRequireDefault(_assessmentQuestionModel);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Questions

function index(req, res) {
  _assessmentQuestionModel2['default'].find().populate('assessment image').sort('questionNumber').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

function fetchBulk(req, res) {
  _assessmentQuestionModel2['default'].find({ _id: { $in: req.body } }).populate('image').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single Question from the DB

function show(req, res) {
  _assessmentQuestionModel2['default'].findById(req.params.id).populate('assessment image').execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

function showAssessmentQuestions(req, res) {
  _assessmentQuestionModel2['default'].find({ "assessment": req.params.assessment }).populate('image').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

function showTest(req, res) {
  _assessmentQuestionModel2['default'].find({ "assessment": req.params.assessment, assessmentQuestionGame: req.params.test }).populate('image').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Question in the DB

function create(req, res) {
  req.body.assessment = ObjectId(req.body.assessment._id);
  if (req.body.image === false) {
    delete req.body.image;
  } else {
    req.body.image = ObjectId(req.body.image._id);
  }
  _assessmentQuestionModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Create new questions from uploaded .CSV file

function bulk(req, res) {
  var toInsert = [];
  var toUpdate = [];
  var _q = req.body.map(function (q, i) {
    q.assessment = ObjectId(q.assessment);
    if (q.image) q.image = ObjectId(q.image);
    q.createdAt = Date.now();
    q.updatedAt = Date.now();
    if (q._id && q._id.length === 24) {
      q._id = ObjectId(q._id);
      toUpdate.push(q);
    } else {
      toInsert.push(q);
    }
    return q;
  });
  if (toUpdate.length > 0) {
    toUpdate.map(function (q, i) {
      _assessmentQuestionModel2['default'].findByIdAsync(q._id).then(handleEntityNotFound(res)).then(saveUpdates(q));
    });
  }
  if (toInsert.length > 0) {
    _assessmentQuestionModel2['default'].collection.insertAsync(toInsert).then(respondWithResult(res, 201))['catch'](handleError(res));
  } else {
    respondWithResult(res, 201)({});
  }
}

// Updates an existing Question in the DB

function update(req, res) {
  if (req.body._id) delete req.body._id;
  if (req.body.$loki) delete req.body.$loki;
  if (req.body.meta) delete req.body.meta;

  req.body.assessment = ObjectId(req.body.assessment._id);
  if (req.body.image) req.body.image = ObjectId(req.body.image._id);
  _assessmentQuestionModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Question from the DB

function destroy(req, res) {
  _assessmentQuestionModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=assessmentQuestion.controller.js.map
