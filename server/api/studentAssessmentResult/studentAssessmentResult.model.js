/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var StudentAssessmentResultSchema = new mongoose.Schema({
  topic: { type: ObjectId, ref: 'Topic' },
  assessment: { type: ObjectId, ref: 'Assessment' },
  assignment: { type: ObjectId, ref: 'ClassroomAssignment' },
  student: { type: ObjectId, ref: 'User' },
  accessCode: { type: String },
  assessmentState: {
    currentPath: { type: String, 'enum': ['A', 'B', 'C', 'D'], 'default': 'A' },
    currentState: { type: String, 'enum': ['NOT_STARTED', 'WAITING', 'NEEDS_PROCEED', 'FINISHED'], 'default': 'NOT_STARTED' },
    currentIndex: { type: String, 'enum': ['0', '1', '2', '3'], 'default': '0' }
  },
  gamePathway: [String],
  result: {
    score: Number,
    workingLevel: Number
  },
  linkedResults: [{ type: ObjectId, ref: 'StudentResult' }],
  startedOn: { type: Date, 'default': Date.now },
  completedAt: Number
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

StudentAssessmentResultSchema.post('save', function (doc, next) {
  doc.populate('topic').populate('assessment').populate('student').populate('linkedResults').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('StudentAssessmentResult', StudentAssessmentResultSchema);
module.exports = exports['default'];
//# sourceMappingURL=studentAssessmentResult.model.js.map
