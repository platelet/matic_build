/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/testResults              ->  index
 * POST    /api/testResults              ->  create
 * GET     /api/testResults/:id          ->  show
 * PUT     /api/testResults/:id          ->  update
 * DELETE  /api/testResults/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.bulkGet = bulkGet;
exports.show = show;
exports.create = create;
exports.start = start;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _studentAssessmentResultModel = require('./studentAssessmentResult.model');

var _studentAssessmentResultModel2 = _interopRequireDefault(_studentAssessmentResultModel);

var _classroomClassroomModel = require('../classroom/classroom.model');

var _classroomClassroomModel2 = _interopRequireDefault(_classroomClassroomModel);

var _assessmentAssessmentModel = require('../assessment/assessment.model');

var _assessmentAssessmentModel2 = _interopRequireDefault(_assessmentAssessmentModel);

var _classroomAssignmentClassroomAssignmentModel = require('../classroomAssignment/classroomAssignment.model');

var _classroomAssignmentClassroomAssignmentModel2 = _interopRequireDefault(_classroomAssignmentClassroomAssignmentModel);

var _workingLevelWorkingLevelModel = require('../workingLevel/workingLevel.model');

var _workingLevelWorkingLevelModel2 = _interopRequireDefault(_workingLevelWorkingLevelModel);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Types.ObjectId;

function filterForUser(user) {
  user = user;
  return function (docs) {
    if (user.userType.title === 'admin' || user.userType.title === 'student') {
      return docs;
    }

    if (typeof docs === 'object' && docs.length) {
      docs = docs.filter(function (e, i) {
        if (e.student && e.student.info) return String(e.student.info.teacher) === String(user._id);else return false;
      });
      return docs;
    } else {
      if (docs.student.info.teacher === user._id) return docs;
    }
  };
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of StudentResults

function index(req, res) {
  if (req.user.userType.title === 'student') {
    var params = { student: req.user._id };
    _studentAssessmentResultModel2['default'].find(params).populate('topic assessment student linkedResults').sort('-completedAt').execAsync().then(respondWithResult(res))['catch'](handleError(res));
  } else if (req.user.userType.title === 'teacher') {
    _classroomClassroomModel2['default'].find({ teacher: req.user._id }).exec().then(function (classrooms) {
      var ids = [];
      classrooms.forEach(function (group) {
        ids = ids.concat(group.students);
      });
      _studentAssessmentResultModel2['default'].find({ student: { $in: ids } }).populate('topic assessment student linkedResults').sort('-completedAt').exec().then(respondWithResult(res))['catch'](handleError(res));
    })['catch'](handleError(res));
  } else {
    var params = {};
    _studentAssessmentResultModel2['default'].find(params).populate('topic assessment student linkedResults').sort('-completedAt').batchSize(100000).execAsync().then(filterForUser(req.user)).then(respondWithResult(res))['catch'](handleError(res));
  }
}

function bulkGet(req, res) {
  var students = req.body.students;
  var objectives = req.body.objectives;

  if (typeof students === 'object' && students.length && students.length !== 0 && typeof objectives === 'object' && objectives.length && objectives.length !== 0) {
    _studentAssessmentResultModel2['default'].find({
      objective: { $in: objectives },
      student: { $in: students }
    }).execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
  } else {
    respondWithResult(res)([]);
  }
}

// Gets a single StudentResult from the DB

function show(req, res) {
  if (req.user.userType.title === 'student') {
    var params = { _id: req.params.id, student: req.user._id };
  } else {
    var params = { _id: req.params.id };
  }
  _studentAssessmentResultModel2['default'].find(params).limit(1).populate('topic assessment student').execAsync().then(handleEntityNotFound(res)).then(filterForUser(req.user)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new StudentResult in the DB

function create(req, res) {
  if (req.user.userType.title === 'student') {
    req.body.accessCode = Math.random().toString(36).slice(-8);
    req.body.student = req.user;
    _studentAssessmentResultModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
  } else {
    respondWithResult(res, 401)(false);
  }
}

function start(req, res) {
  if (req.user.userType.title === 'student') {

    _assessmentAssessmentModel2['default'].findOne({ _id: req.params.assessmentId }).populate('topic').exec().then(function (assessment) {
      if (assessment) {
        var _result = {
          topic: ObjectId(assessment.topic._id),
          assessment: ObjectId(assessment._id),
          student: ObjectId(req.user._id),
          accessCode: Math.random().toString(36).slice(-8)
        };
        _studentAssessmentResultModel2['default'].createAsync(_result).then(respondWithResult(res, 201))['catch'](handleError(res));
      } else {
        respondWithResult(res, 401)(false);
      }
    });
  } else {
    respondWithResult(res, 401)(false);
  }
}

// Updates an existing StudentResult in the DB

function update(req, res) {
  var assignmentNeedsUpdate = req.body.assessmentState.currentState === 'FINISHED';
  var assignmentId = req.body.assignmentid;
  if (assignmentNeedsUpdate) {
    delete req.body.assignmentid;
  }
  _studentAssessmentResultModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(function (assessmentResult) {
    if (assignmentNeedsUpdate) {
      _workingLevelWorkingLevelModel2['default'].find({ student: req.user, topic: req.body.topic }).exec().then(function (workingLevel) {
        if (workingLevel && workingLevel[0]) {
          workingLevel[0].workingLevel = req.body.result.workingLevel;
          workingLevel[0].assessmentLink = ObjectId(req.params.id);
          workingLevel[0].assignmentMethod = 'STUDENT_ASSESSMENT';
          workingLevel[0].save();
        } else {
          var newWorkingLevel = new _workingLevelWorkingLevelModel2['default']({
            topic: req.body.topic,
            student: req.user,
            workingLevel: req.body.result.workingLevel,
            assessmentLink: ObjectId(req.params.id),
            assignmentMethod: 'STUDENT_ASSESSMENT'
          });
          newWorkingLevel.save();
        }
      });
      _classroomAssignmentClassroomAssignmentModel2['default'].findByIdAsync(assignmentId).then(function (assignment) {
        var linkedResults = assignment.linkedAssessmentResults;
        linkedResults.push(assessmentResult);
        assignment.linkedResults = linkedResults;
        assignment.save();
        return assessmentResult;
      }).then(respondWithResult(res))['catch'](handleError(res));
    } else {
      respondWithResult(res)(assessmentResult);
    }
  })['catch'](handleError(res));
}

// Deletes a StudentResult from the DB

function destroy(req, res) {
  _studentAssessmentResultModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=studentAssessmentResult.controller.js.map
