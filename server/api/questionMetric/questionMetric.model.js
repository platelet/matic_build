/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var QuestionMetricSchema = new mongoose.Schema({
  question: { type: ObjectId, ref: 'Question' },
  answer: String,
  count: Number
});

exports['default'] = mongoose.model('QuestionMetric', QuestionMetricSchema);
module.exports = exports['default'];
//# sourceMappingURL=questionMetric.model.js.map
