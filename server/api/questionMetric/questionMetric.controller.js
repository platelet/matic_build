/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/questionMetrics              ->  index
 * POST    /api/questionMetrics              ->  create
 * GET     /api/questionMetrics/:id          ->  show
 * PUT     /api/questionMetrics/:id          ->  update
 * DELETE  /api/questionMetrics/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _questionMetricModel = require('./questionMetric.model');

var _questionMetricModel2 = _interopRequireDefault(_questionMetricModel);

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of QuestionMetrics

function index(req, res) {
  _questionMetricModel2['default'].find().populate('question').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single QuestionMetric from the DB

function show(req, res) {
  _questionMetricModel2['default'].findById(req.params.id).populate('question').execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new QuestionMetric in the DB

function create(req, res) {
  _questionMetricModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Updates an existing QuestionMetric in the DB

function update(req, res) {
  if (req.body._id) delete req.body._id;
  if (req.body.$loki) delete req.body.$loki;
  if (req.body.meta) delete req.body.meta;

  _questionMetricModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a QuestionMetric from the DB

function destroy(req, res) {
  _questionMetricModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=questionMetric.controller.js.map
