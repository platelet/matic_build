/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/studentProgress         ->  index
 * POST    /api/studentProgress         ->  create
 * GET     /api/studentProgress/:id     ->  show
 * PUT     /api/studentProgress/:id     ->  update
 * DELETE  /api/studentProgress/:id     ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _studentProgressModel = require('./studentProgress.model');

var _studentProgressModel2 = _interopRequireDefault(_studentProgressModel);

var _classroomClassroomModel = require('../classroom/classroom.model');

var _classroomClassroomModel2 = _interopRequireDefault(_classroomClassroomModel);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of StudentResults

function index(req, res) {
  if (req.user.userType.title === 'student') {
    var params = { student: req.user._id };
    _studentProgressModel2['default'].find(params).populate('topic student').sort('-timestamp').execAsync().then(respondWithResult(res))['catch'](handleError(res));
  } else if (req.user.userType.title === 'teacher') {
    _classroomClassroomModel2['default'].find({ teacher: req.user._id }).exec().then(function (classrooms) {
      var ids = [];
      classrooms.forEach(function (group) {
        ids = ids.concat(group.students);
      });
      _studentProgressModel2['default'].find({ student: { $in: ids } }).populate('topic student').sort('-timestamp').exec().then(respondWithResult(res))['catch'](handleError(res));
    })['catch'](handleError(res));
  } else {
    var params = {};
    _studentProgressModel2['default'].find(params).populate('topic student').sort('-timestamp').batchSize(100000).execAsync().then(respondWithResult(res))['catch'](handleError(res));
  }
}

// Gets a single StudentResult from the DB

function show(req, res) {
  if (req.user.userType.title === 'student') {
    var params = { _id: req.params.id, student: req.user._id };
  } else {
    var params = { _id: req.params.id };
  }
  _studentProgressModel2['default'].find(params).limit(1).populate('topic student').execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new StudentResult in the DB

function create(req, res) {
  if (req.user.userType.title === 'student') {
    _studentProgressModel2['default'].findOneAndUpdate({
      student: req.user,
      activeProgress: true
    }, { $set: {
        activeProgress: false
      } }, function (err, doc) {
      req.body.student = req.user;
      _studentProgressModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
    });
  } else {
    respondWithResult(res, 401)(false);
  }
}

// Updates an existing StudentResult in the DB

function update(req, res) {
  _studentProgressModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a StudentResult from the DB

function destroy(req, res) {
  _studentProgressModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=studentProgress.controller.js.map
