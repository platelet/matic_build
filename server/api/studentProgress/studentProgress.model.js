/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _configEnvironment = require('../../config/environment');

var _configEnvironment2 = _interopRequireDefault(_configEnvironment);

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var StudentProgressSchema = new mongoose.Schema({
  student: { type: ObjectId, ref: 'User' },
  topic: { type: ObjectId, ref: 'Topic' },
  objectives: {}, //1 --> G/S/B, 50 --> G/S/B
  activeProgress: { type: Boolean, 'default': true },
  timestamp: { type: Date, 'default': Date.now }
});

StudentProgressSchema.post('save', function (doc, next) {
  doc.populate('student').populate('topic').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('StudentProgress', StudentProgressSchema);
module.exports = exports['default'];
//# sourceMappingURL=studentProgress.model.js.map
