/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var DashboardMessageSchema = new mongoose.Schema({
  userType: { type: ObjectId, ref: 'UserType' },
  message: { type: String },
  isActive: { type: Boolean, 'default': true },
  createdAt: { type: Date, defualt: Date.now }
});

DashboardMessageSchema.post('save', function (doc, next) {
  doc.populate('userType').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('DashboardMessage', DashboardMessageSchema);
module.exports = exports['default'];
//# sourceMappingURL=dashboardMessage.model.js.map
