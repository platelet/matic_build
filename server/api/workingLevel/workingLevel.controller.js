/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _classroomClassroomModel = require('../classroom/classroom.model');

var _classroomClassroomModel2 = _interopRequireDefault(_classroomClassroomModel);

var _workingLevelModel = require('./workingLevel.model');

var _workingLevelModel2 = _interopRequireDefault(_workingLevelModel);

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Files

function index(req, res) {
  var userType = undefined;
  if (req.user.userType && req.user.userType.title) {
    userType = req.user.userType.title;
  } else {
    handleError(res, 402)('ACCESS_DENIED');
    return;
  }

  if (userType === 'student') {
    _workingLevelModel2['default'].find({
      student: req.user
    }).populate('student topic assessmentLink').execAsync().then(respondWithResult(res))['catch'](handleError(res));
  } else if (userType === 'teacher') {
    _classroomClassroomModel2['default'].find({ teacher: req.user }).populate('students').execAsync().then(function (classrooms) {
      if (classrooms && classrooms[0]) {
        var studentIds = classrooms[0].students.map(function (su) {
          return su._id;
        });
        _workingLevelModel2['default'].find({ student: studentIds }).populate('student topic assessmentLink').execAsync().then(respondWithResult(res))['catch'](handleError(res));
      } else {
        handleError(res, 402)('ACCESS_DENIED');
      }
    })['catch'](handleError(res));
  } else if (userType === 'admin') {
    _workingLevelModel2['default'].find().populate('student topic assessmentLink').execAsync().then(respondWithResult(res))['catch'](handleError(res));
  }
}

// Gets a single File from the DB

function show(req, res) {
  _workingLevelModel2['default'].findById(req.params.id).execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new File in the DB

function create(req, res) {
  _workingLevelModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Updates an existing File in the DB

function update(req, res) {
  _workingLevelModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a File from the DB

function destroy(req, res) {
  _workingLevelModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=workingLevel.controller.js.map
