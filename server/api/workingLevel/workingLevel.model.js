/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var WorkingLevelSchema = new mongoose.Schema({
  student: { type: ObjectId, ref: 'User' },
  topic: { type: ObjectId, ref: 'Topic' },
  workingLevel: { type: Number, 'default': 1 },
  assessmentLink: { type: ObjectId, ref: 'StudentAssessmentResult' },
  assignmentMethod: { type: String, 'enum': ['STUDENT_ASSESSMENT', 'TEACHER_MANUAL', 'STUDENT_PROGRESSION', 'ADMIN_MANUAL'] },
  createdAt: { type: Date, 'default': Date.now }
});

WorkingLevelSchema.post('save', function (doc, next) {
  doc.populate('topic').populate('student').populate('assessmentLink').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('WorkingLevel', WorkingLevelSchema);
module.exports = exports['default'];
//# sourceMappingURL=workingLevel.model.js.map
