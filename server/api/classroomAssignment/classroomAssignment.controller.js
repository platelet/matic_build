/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/classrooms         ->  index
 * POST    /api/classrooms         ->  create
 * GET     /api/classrooms/:id     ->  show
 * PUT     /api/classrooms/:id     ->  update
 * DELETE  /api/classrooms/:id     ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _classroomClassroomModel = require('../classroom/classroom.model');

var _classroomClassroomModel2 = _interopRequireDefault(_classroomClassroomModel);

var _classroomAssignmentModel = require('./classroomAssignment.model');

var _classroomAssignmentModel2 = _interopRequireDefault(_classroomAssignmentModel);

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Feedbacks

function index(req, res) {
  if (req.user.userType.title === 'teacher') {
    _classroomClassroomModel2['default'].find({ teacher: req.user }).execAsync().then(function (classroom) {
      if (classroom[0]) {
        _classroomAssignmentModel2['default'].find({ classroom: classroom[0] }).populate('linkedTopic linkedSurvey linkedActivity linkedSurveyResponses linkedAssessmentResults').populate({
          path: 'linkedAssessment',
          populate: {
            path: 'topic',
            model: 'Topic'
          }
        }).execAsync().then(respondWithResult(res))['catch'](handleError(res));
      }
    });
  } else if (req.user.userType.title === 'student') {
    _classroomClassroomModel2['default'].find({ students: req.user._id }).execAsync().then(function (classroom) {
      if (classroom[0]) {
        _classroomAssignmentModel2['default'].find({ classroom: classroom[0], isActive: true }).populate('linkedTopic linkedSurvey linkedActivity linkedSurveyResponses linkedAssessmentResults').populate({
          path: 'linkedAssessment',
          populate: {
            path: 'topic',
            model: 'Topic'
          }
        }).execAsync().then(respondWithResult(res))['catch'](handleError(res));
      }
    });
  } else {
    _classroomAssignmentModel2['default'].find().populate('linkedTopic linkedSurvey linkedActivity linkedSurveyResponses linkedAssessmentResults').populate({
      path: 'linkedAssessment',
      populate: {
        path: 'topic',
        model: 'Topic'
      }
    }).execAsync().then(respondWithResult(res))['catch'](handleError(res));
  }
}

// Gets a single Feedback from the DB

function show(req, res) {
  _classroomAssignmentModel2['default'].findByIdAsync(req.params.id).populate('linkedTopic linkedSurvey linkedActivity linkedSurveyResponses linkedAssessmentResults').populate({
    path: 'linkedAssessment',
    populate: {
      path: 'topic',
      model: 'Topic'
    }
  }).then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Assessment in the DB

function create(req, res) {
  _classroomAssignmentModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Updates an existing Feedback in the DB

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  _classroomAssignmentModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Feedback from the DB

function destroy(req, res) {
  _classroomAssignmentModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=classroomAssignment.controller.js.map
