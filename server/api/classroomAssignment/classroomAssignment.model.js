/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var ClassroomAssignmentSchema = new mongoose.Schema({
  classroom: { type: ObjectId, ref: 'Classroom' },
  assignmentType: { type: String, 'enum': ['TOPIC', 'SURVEY', 'ACTIVITY', 'ASSESSMENT', 'WORD_PROBLEM'], 'default': 'TOPIC' },
  linkedTopic: { type: ObjectId, ref: 'Topic' },
  linkedSurvey: { type: ObjectId, ref: 'Survey' },
  linkedActivity: { type: ObjectId, ref: 'Activity' },
  linkedAssessment: { type: ObjectId, ref: 'Assessment' },
  assignmentPeriod: {
    dateStart: { type: Date, 'default': Date.now },
    dateEnd: { type: Date, 'default': Date.now }
  },
  linkedResults: [{ type: ObjectId, ref: 'StudentResult' }],
  linkedAssessmentResults: [{ type: ObjectId, ref: 'StudentAssessmentResult' }],
  linkedSurveyResponses: [{ type: ObjectId, ref: 'SurveyResponse' }],
  attemptLimit: { type: Number, 'default': 1 },
  isActive: { type: Boolean, 'default': true }
}, {
  usePushEach: true
});

ClassroomAssignmentSchema.post('save', function (doc, next) {
  doc.populate('linkedTopic linkedSurvey linkedActivity linkedAssessment linkedSurveyResponses linkedAssessmentResults').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('ClassroomAssignment', ClassroomAssignmentSchema);
module.exports = exports['default'];
//# sourceMappingURL=classroomAssignment.model.js.map
