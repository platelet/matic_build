/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _shaJs = require('sha.js');

var _shaJs2 = _interopRequireDefault(_shaJs);

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

var UserSchema = new mongoose.Schema({
  user: { type: String, required: true },
  classroom: {},
  email: { type: String, unique: true },
  stripeCustomer: { type: String, 'default': '' },
  subscription: {
    subid: {},
    subdate: {}
  },
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Document Level Methods
 */
UserSchema.post('save', function (doc, next) {
  doc.populate('userType classroom teacher').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('User', UserSchema);
module.exports = exports['default'];
//# sourceMappingURL=classSubscription.model.js.map
