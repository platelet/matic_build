/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

var WordProblemCategorySchema = new mongoose.Schema({
  name: { type: String },
  icon: { type: String, 'default': 'plus_minus' },
  color: { type: String, 'default': 'green' },
  enabled: { type: Boolean, 'default': true },
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

exports['default'] = mongoose.model('WordProblemCategory', WordProblemCategorySchema);
module.exports = exports['default'];
//# sourceMappingURL=wordProblemCategory.model.js.map
