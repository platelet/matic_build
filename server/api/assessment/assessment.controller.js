/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/feedbacks              ->  index
 * POST    /api/feedbacks              ->  create
 * GET     /api/feedbacks/:id          ->  show
 * PUT     /api/feedbacks/:id          ->  update
 * DELETE  /api/feedbacks/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _assessmentModel = require('./assessment.model');

var _assessmentModel2 = _interopRequireDefault(_assessmentModel);

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Feedbacks

function index(req, res) {
  var search = undefined;
  if (req.user.userType.title === 'admin') {
    search = {};
  } else {
    search = { active: true };
  }
  _assessmentModel2['default'].find(search).populate('topic').populate('games.A1 games.A2 game.A3 games.B2 games.B3 games.C2 games.C3 games.D2 games.D3').populate({ path: 'games.A1', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.A2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.A3', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.B2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.B3', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.C2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.C3', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.D2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.D3', populate: { path: 'game', model: 'Game' } }).execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single Feedback from the DB

function show(req, res) {
  _assessmentModel2['default'].findByIdAsync(req.params.id).populate('topic').populate('games.A1 games.A2 game.A3 games.B2 games.B3 games.C2 games.C3 games.D2 games.D3').populate({ path: 'games.A1', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.A2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.A3', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.B2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.B3', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.C2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.C3', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.D2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.D3', populate: { path: 'game', model: 'Game' } }).then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Assessment in the DB

function create(req, res) {
  _assessmentModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Updates an existing Feedback in the DB

function update(req, res) {
  _assessmentModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Feedback from the DB

function destroy(req, res) {
  _assessmentModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=assessment.controller.js.map
