/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var AssessmentSchema = new mongoose.Schema({
  name: { type: String, 'default': 'New Assessment' },
  topic: { type: ObjectId, ref: 'Topic' },
  games: {
    A1: { type: ObjectId, ref: 'GameLevel' },
    A2: { type: ObjectId, ref: 'GameLevel' },
    A3: { type: ObjectId, ref: 'GameLevel' },
    B2: { type: ObjectId, ref: 'GameLevel' },
    B3: { type: ObjectId, ref: 'GameLevel' },
    C2: { type: ObjectId, ref: 'GameLevel' },
    C3: { type: ObjectId, ref: 'GameLevel' },
    D2: { type: ObjectId, ref: 'GameLevel' },
    D3: { type: ObjectId, ref: 'GameLevel' }
  },
  gameTiming: {
    A1: { type: Number, 'default': 60 },
    A2: { type: Number, 'default': 60 },
    A3: { type: Number, 'default': 60 },
    B2: { type: Number, 'default': 60 },
    B3: { type: Number, 'default': 60 },
    C2: { type: Number, 'default': 60 },
    C3: { type: Number, 'default': 60 },
    D2: { type: Number, 'default': 60 },
    D3: { type: Number, 'default': 60 }
  },
  conditionals: {
    AA: { operand: { type: String, 'enum': ['lt', 'gt', 'bt', 'eq'], 'default': 'lt' }, conditionValue: { type: Number, 'default': 12 }, betweenValue: Number },
    AB: { operand: { type: String, 'enum': ['lt', 'gt', 'bt', 'eq'], 'default': 'gt' }, conditionValue: { type: Number, 'default': 12 }, betweenValue: Number },
    BC: { operand: { type: String, 'enum': ['lt', 'gt', 'bt', 'eq'], 'default': 'bt' }, conditionValue: { type: Number, 'default': 20 }, betweenValue: { type: Number, 'default': 30 } },
    BD: { operand: { type: String, 'enum': ['lt', 'gt', 'bt', 'eq'], 'default': 'gt' }, conditionValue: { type: Number, 'default': 30 }, betweenValue: Number }
  },
  assessmentObjectives: {
    levels: [String],
    objectives: [String]
  },
  icon: String,
  color: { type: String, 'default': 'blue' },
  description: { type: String, 'default': '' },
  teacherDescription: { type: String, 'default': '' },
  studentDescription: { type: String, 'default': '' },
  active: { type: Boolean, 'default': true },
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

AssessmentSchema.post('save', function (doc, next) {
  doc.populate('topic').populate('games.A1 games.A2 game.A3 games.B2 games.B3 games.C2 games.C3 games.D2 games.D3').populate({ path: 'games.A1', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.A2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.A3', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.B2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.B3', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.C2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.C3', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.D2', populate: { path: 'game', model: 'Game' } }).populate({ path: 'games.D3', populate: { path: 'game', model: 'Game' } }).execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('Assessment', AssessmentSchema);
module.exports = exports['default'];
//# sourceMappingURL=assessment.model.js.map
