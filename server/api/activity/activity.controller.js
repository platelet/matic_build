/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/activites            ->  index
 * POST    /api/activites            ->  create
 * GET     /api/activites/:id        ->  show
 * PUT     /api/activites/:id        ->  update
 * DELETE  /api/activites/:id        ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _activityModel = require('./activity.model');

var _activityModel2 = _interopRequireDefault(_activityModel);

function userPerms(user, perms) {
  if (user && user.userType) {
    if (typeof perms === "string") {
      return user.userType.title === perms;
    } else if (typeof perms === "object" && perms.length) {
      return perms.indexOf(user.userType.title) !== -1;
    }
  }
  return false;
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Activities

function index(req, res) {
  if (userPerms(req.user, 'admin')) {
    _activityModel2['default'].find().populate('mainImage files').populate('sections.image').execAsync().then(respondWithResult(res))['catch'](handleError(res));
  } else {
    _activityModel2['default'].find().populate('mainImage files').populate('sections.image').execAsync().then(respondWithResult(res))['catch'](handleError(res));
  }
}

// Gets a single Survey from the DB

function show(req, res) {
  _activityModel2['default'].findById(req.params.id).populate('mainImage files images').execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Activity in the DB

function create(req, res) {
  _activityModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Updates an existing Activity in the DB

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  _activityModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Activity from the DB

function destroy(req, res) {
  _activityModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=activity.controller.js.map
