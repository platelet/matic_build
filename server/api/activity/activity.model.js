/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

var ActivitySchema = new mongoose.Schema({
  name: { type: String, 'default': '' },
  title: { type: String, 'default': '' },
  mainImage: { type: ObjectId, ref: 'Image' },
  blurb: { type: String, 'default': '' },
  sections: [{
    image: { type: ObjectId, ref: 'Image' },
    sectionTitle: { type: String, 'default': '' },
    description: { type: String, 'default': '' }
  }],
  files: [{ type: ObjectId, ref: 'File' }],
  icon: String,
  color: { type: String, 'default': 'blue' },
  description: { type: String, 'default': '' },
  teacherDescription: { type: String, 'default': '' },
  studentDescription: { type: String, 'default': '' },
  enabled: { type: Boolean, 'default': true }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Document Level Methods
 */
ActivitySchema.post('save', function (doc, next) {
  doc.populate('mainImage').populate('sections.image').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('Activity', ActivitySchema);
module.exports = exports['default'];
//# sourceMappingURL=activity.model.js.map
