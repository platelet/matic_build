/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var InstitutionSchema = new mongoose.Schema({
  name: String,
  address: String,
  contactNumber: String,
  contactEmail: String,
  contactPerson: String,
  user: { type: ObjectId, ref: 'User' },
  associatedTeachers: [{
    teacher: { type: ObjectId, ref: 'User' },
    classroom: String
  }],
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

exports['default'] = mongoose.model('Institution', InstitutionSchema);
module.exports = exports['default'];
//# sourceMappingURL=institution.model.js.map
