/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var ClassroomSchema = new mongoose.Schema({
  teacher: { type: ObjectId, ref: 'User' },
  students: [{ type: ObjectId, ref: 'User' }],
  classroomName: { type: String, 'default': 'New Classroom' },
  classroomNumber: { type: String, defualt: 'R1' },
  school: String
});

ClassroomSchema.post('save', function (doc, next) {
  doc.populate('teacher').populate('students').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('Classroom', ClassroomSchema);
module.exports = exports['default'];
//# sourceMappingURL=classroom.model.js.map
