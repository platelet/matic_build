/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/classrooms         ->  index
 * POST    /api/classrooms         ->  create
 * GET     /api/classrooms/:id     ->  show
 * PUT     /api/classrooms/:id     ->  update
 * DELETE  /api/classrooms/:id     ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _studentGroupStudentGroupModel = require('../studentGroup/studentGroup.model');

var _studentGroupStudentGroupModel2 = _interopRequireDefault(_studentGroupStudentGroupModel);

var _userUserModel = require('../user/user.model');

var _userUserModel2 = _interopRequireDefault(_userUserModel);

var _classroomModel = require('./classroom.model');

var _classroomModel2 = _interopRequireDefault(_classroomModel);

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Classrooms

function index(req, res) {
  var search = {};
  if (req.user.userType.title === 'student') {
    search.students = { $contain: req.user._id };
  } else if (req.user.userType.title === 'teacher') {
    search.teacher = req.user._id;
  }
  _classroomModel2['default'].find(search).populate('teacher').populate('students').populate('activeAssignments').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single Feedback from the DB

function show(req, res) {
  _classroomModel2['default'].findByIdAsync(req.params.id).populate('teacher').populate('students').populate('activeAssignments').then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Assessment in the DB

function create(req, res) {
  if (req.user.userType.title === 'admin') {
    (function () {
      // req.body = {
      //   teacher : ...,
      //   students : [],
      //   groups   : []
      // }
      var studentIds = req.body.students;
      var groupIds = req.body.groups;
      var classroom = undefined;

      _classroomModel2['default'].createAsync({
        teacher: req.body.teacher,
        students: req.body.students,
        classroomName: req.body.teacher.fullname + "'s Classroom'"
      }).then(function (_classroom) {
        classroom = _classroom;
        if (classroom && classroom._id) {
          var studentUpdate = {
            teacher: req.body.teacher._id,
            classroom: classroom._id
          };
          _userUserModel2['default'].updateMany({ _id: { $in: studentIds } }, studentUpdate).then(function (userSaveRes) {
            _studentGroupStudentGroupModel2['default'].deleteMany({ _id: { $in: groupIds } }).then(function (studentGroupDeleteRes) {
              return classroom;
            }).then(respondWithResult(res, 201));
          });
        }
      })['catch'](handleError(res));
    })();
  } else {
    _classroomModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
  }
}

// Updates an existing Feedback in the DB

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  _classroomModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Feedback from the DB

function destroy(req, res) {
  _classroomModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=classroom.controller.js.map
