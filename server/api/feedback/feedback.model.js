/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var FeedbackSchema = new mongoose.Schema({
  user: {
    type: ObjectId,
    ref: 'User'
  },
  message: String,
  sentAt: { type: Date, 'default': Date.now }
});

exports['default'] = mongoose.model('Feedback', FeedbackSchema);
module.exports = exports['default'];
//# sourceMappingURL=feedback.model.js.map
