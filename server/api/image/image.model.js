/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var ImageSchema = new mongoose.Schema({
  name: String,
  category: String,
  awsBucket: String,
  awsFolder: String,
  awsBucketKey: String,
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

ImageSchema.virtual('url').get(function () {
  var baseUrl = 'https://s3-ap-southeast-2.amazonaws.com/';
  return baseUrl + this.awsBucket + '/' + this.awsFolder + '/' + encodeURIComponent(this.awsBucketKey);
});

exports['default'] = mongoose.model('Image', ImageSchema);
module.exports = exports['default'];
//# sourceMappingURL=image.model.js.map
