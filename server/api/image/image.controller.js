/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/images              ->  index
 * POST    /api/images              ->  create
 * GET     /api/images/:id          ->  show
 * PUT     /api/images/:id          ->  update
 * DELETE  /api/images/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _imageModel = require('./image.model');

var _imageModel2 = _interopRequireDefault(_imageModel);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _s3 = require('s3');

var _s32 = _interopRequireDefault(_s3);

var _configAwsJs = require('../../config/aws.js');

var _configAwsJs2 = _interopRequireDefault(_configAwsJs);

var s3Client = _s32['default'].createClient({
  maxAsyncS3: 20,
  s3RetryCount: 3,
  s3RetryDelay: 1000,
  multipartUploadThreshold: 20971520,
  multipartUploadSize: 15728640,
  s3Options: {
    accessKeyId: "AKIAJW3755B3KI3KXEPA",
    secretAccessKey: "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"
  }
});

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removes3Asset(entity) {
  var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
  if (env === 'development') {
    var bucket = 'math-adv-matic-dev';
  } else {
    var bucket = 'math-adv-matic';
  }
  var params = {
    Bucket: bucket, /* required */
    Delete: {
      Objects: [{
        Key: entity.awsFolder + "/" + entity.awsBucketKey
      }]
    }
  };
  s3Client.deleteObjects(params, function (err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else console.log(data); // successful response
  });
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      removes3Asset(entity);
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Images

function index(req, res) {
  _imageModel2['default'].find({}).execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single Image from the DB

function show(req, res) {
  _imageModel2['default'].findById(req.params.id).execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Image in the DB

function create(req, res) {
  var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
  if (env === 'development') {
    var bucket = "math-adv-matic-dev";
  } else {
    var bucket = "math-adv-matic";
  }
  var images = req.body.files.map(function (img, i) {
    var extension = img.detail.path.match(/\.[0-9a-z]+$/i)[0];
    return {
      name: img.name,
      awsBucket: bucket,
      awsFolder: "images",
      awsBucketKey: img.detail.base + extension,
      category: img.category
    };
  });
  var params = {
    localDir: _path2['default'].join(__dirname + '/../../tmp_files'),
    s3Params: {
      Prefix: "images",
      Bucket: bucket,
      ACL: 'public-read'
    }
  };
  var uploader = s3Client.uploadDir(params);
  uploader.on('progress', function () {
    console.log("progress", uploader.progressMd5Amount, uploader.progressAmount, uploader.progressTotal);
  });
  uploader.on('error', function (err) {
    console.error("unable to upload:", err.stack);
  });
  uploader.on('end', function () {
    try {
      var files = _fs2['default'].readdirSync(_path2['default'].join(__dirname + '/../../tmp_files'));
    } catch (e) {
      return;
    }
    if (files.length > 0) for (var i = 0; i < files.length; i++) {
      var filePath = _path2['default'].join(__dirname + '/../../tmp_files/', files[i]);
      if (_fs2['default'].statSync(filePath).isFile()) _fs2['default'].unlinkSync(filePath);
    }
    _imageModel2['default'].createAsync(images).then(respondWithResult(res, 201))['catch'](handleError(res));
  });
}

// Updates an existing Subject in the DB

function update(req, res) {
  if (req.body.awsBucket) delete req.body.awsBucket;
  if (req.body.awsFolder) delete req.body.awsFolder;
  if (req.body.awsBucketKey) delete req.body.awsBucketKey;
  if (req.body._id) delete req.body._id;

  _imageModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Image from the DB

function destroy(req, res) {
  _imageModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=image.controller.js.map
