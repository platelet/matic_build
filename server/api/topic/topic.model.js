/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _configEnvironment = require('../../config/environment');

var _configEnvironment2 = _interopRequireDefault(_configEnvironment);

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var TopicSchema = new mongoose.Schema({
  name: { type: String, 'default': 'New Topic' },
  icon: {
    type: String,
    'enum': _configEnvironment2['default'].appIcons
  },
  description: { type: String, 'default': '' },
  teacherDescription: { type: String, 'default': '' },
  studentDescription: { type: String, 'default': '' },
  color: {
    type: String,
    'enum': _configEnvironment2['default'].colors,
    'default': 'green'
  },
  isDefault: { type: Boolean, 'default': false },
  enabled: { type: Boolean, 'default': true },
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

exports['default'] = mongoose.model('Topic', TopicSchema);
module.exports = exports['default'];
//# sourceMappingURL=topic.model.js.map
