/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/topics              ->  index
 * POST    /api/topics              ->  create
 * GET     /api/topics/:id          ->  show
 * PUT     /api/topics/:id          ->  update
 * DELETE  /api/topics/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _topicModel = require('./topic.model');

var _topicModel2 = _interopRequireDefault(_topicModel);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Topics

function index(req, res) {
  if (req.user && req.user._id) {
    if (req.user.userType.title === 'teacher' || req.user.userType.title === 'student') {
      _topicModel2['default'].find({ enabled: true }).execAsync().then(respondWithResult(res))['catch'](handleError(res));
    } else {
      _topicModel2['default'].find({}).execAsync().then(respondWithResult(res))['catch'](handleError(res));
    }
  } else {
    var search = { publicDemo: true };
    _topicModel2['default'].find(search).execAsync().then(respondWithResult(res))['catch'](handleError(res));
  }
}

// Gets a single Topic from the DB

function show(req, res) {
  _topicModel2['default'].findById(req.params.id).execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Topic in the DB

function create(req, res) {
  _topicModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Updates an existing Topic in the DB

function update(req, res) {
  if (req.body._id) delete req.body._id;
  if (req.body.$loki) delete req.body.$loki;
  if (req.body.meta) delete req.body.meta;
  _topicModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Topic from the DB

function destroy(req, res) {
  _topicModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=topic.controller.js.map
