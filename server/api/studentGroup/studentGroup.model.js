/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var StudentGroupSchema = new mongoose.Schema({
  teacher: { type: ObjectId, ref: 'User' },
  students: [{ type: ObjectId, ref: 'User' }],
  name: { type: String, 'default': 'Student Group' },
  groupIndex: { type: Number, 'default': 1 },
  color: { type: String, 'default': 'green' },
  linkedTopics: [{ type: ObjectId, ref: 'Topic' }],
  isDefault: { type: Boolean, 'default': false }
});

StudentGroupSchema.post('save', function (doc, next) {
  doc.populate('teacher').populate('students').populate('linkedTopics').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('StudentGroup', StudentGroupSchema);
module.exports = exports['default'];
//# sourceMappingURL=studentGroup.model.js.map
