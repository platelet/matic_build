/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/studentGroups         ->  index
 * GET     /api/studentGroups/teacher -> teacherGroups
 * GET     /api/studentGroups/student -> studentGroup
 * POST    /api/studentGroups         ->  create
 * GET     /api/studentGroups/:id     ->  show
 * POST    /api/studentGroups/:id/student/:userId/add -> addStudent
 * POST    /api/studentGroups/:id/student/:userId/add -> removeStudent
 * PUT     /api/studentGroups/:id     ->  update
 * DELETE  /api/studentGroups/:id     ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.associateStudents = associateStudents;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _studentGroupModel = require('./studentGroup.model');

var _studentGroupModel2 = _interopRequireDefault(_studentGroupModel);

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    if (updates.linkedTopics) updated.linkedTopics = updates.linkedTopics;
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of StudentGroups

function index(req, res) {
  var search = {};
  if (req.user.userType.title === 'student') {
    search = { students: req.user._id };
  } else if (req.user.userType.title === 'teacher') {
    search = { teacher: req.user._id };
  }
  _studentGroupModel2['default'].find(search).populate('teacher').populate('students').populate('linkedTopics').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

function associateStudents(req, res) {
  console.log('STARTING STUDENT GROUP ASSOC');
  _studentGroupModel2['default'].find({}).select('teacher students').populate('teacher').populate('students').execAsync().then(function (groups) {
    console.log('GROUPS FOUND:', groups.length);
    groups.forEach(function (group) {
      console.log('Working through student group:', group.name);
      if (group.teacher) {
        console.log('Teacher', group.teacher.fullname);
        console.log('Group Student Count:', group.students.length);
        group.students.forEach(function (student) {
          if (student.info && student.info.teacher) {
            if (student.info.teacher === group.teacher._id) {
              console.log('student teacher link ok');
            } else {
              student.info = { teacher: group.teacher._id };
              student.save();
              console.log('student updated');
            }
          } else {
            student.info = { teacher: group.teacher._id };
            student.save();
            console.log('student updated');
          }
        });
        console.log('STUDENTS FINISHED');
        group.save();
        console.log('GROUP SAVED -- NEXT');
      } else {
        console.log('NO TEACHER FOUND -- EXIT');
      }
    });
    console.log('ALL GROUPS FINISHED');
    return true;
  }).then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single StudentGroups from the DB

function show(req, res) {
  _studentGroupModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new StudentGroup in the DB

function create(req, res) {
  if (!req.body.teacher) {
    req.body.teacher = req.user;
  }
  _studentGroupModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Updates an existing StudentGroup in the DB

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  _studentGroupModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a StudentGroup from the DB

function destroy(req, res) {
  _studentGroupModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=studentGroup.controller.js.map
