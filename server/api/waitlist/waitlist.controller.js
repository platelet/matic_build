/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/waitlists              ->  index
 * POST    /api/waitlists              ->  create
 * GET     /api/waitlists/:id          ->  show
 * PUT     /api/waitlists/:id          ->  update
 * POST    /api/waitlists/:id/on       ->  onboard
 * DELETE  /api/waitlists/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.onboard = onboard;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _waitlistModel = require('./waitlist.model');

var _waitlistModel2 = _interopRequireDefault(_waitlistModel);

var _mail = require('../../mail');

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Waitlist

function index(req, res) {
  _waitlistModel2['default'].findAsync().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single Waitlist from the DB

function show(req, res) {
  _waitlistModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Waitlist in the DB

function create(req, res) {
  req.body.onBoardCode = Math.random().toString(36).slice(-6);
  _waitlistModel2['default'].createAsync(req.body).then(function (w) {
    (0, _mail.sendWaitlistedEmail)(w.email);
    return w;
  }).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Updates an existing Waitlist in the DB

function update(req, res) {
  if (req.body._id) delete req.body._id;
  if (req.body.$loki) delete req.body.$loki;
  if (req.body.meta) delete req.body.meta;

  _waitlistModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

function onboard(req, res) {
  _waitlistModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Waitlist from the DB

function destroy(req, res) {
  _waitlistModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=waitlist.controller.js.map
