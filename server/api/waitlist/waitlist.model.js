/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var WaitlistSchema = new mongoose.Schema({
  email: String,
  status: {
    initialEmail: { type: Boolean, 'default': false },
    onBoardEmail: { type: Boolean, 'default': false },
    accountMade: { type: Boolean, 'default': false }
  },
  onBoardCode: { type: String, 'default': '' },
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

exports['default'] = mongoose.model('Waitlist', WaitlistSchema);
module.exports = exports['default'];
//# sourceMappingURL=waitlist.model.js.map
