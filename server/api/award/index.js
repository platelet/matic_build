/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _componentsEnsureApiUser = require('../../components/ensureApiUser');

var _componentsEnsureApiUser2 = _interopRequireDefault(_componentsEnsureApiUser);

var express = require('express');
var controller = require('./award.controller');

var router = express.Router();
var ensureAPIUser = _componentsEnsureApiUser2['default'].ensureAPIUser;

exports['default'] = function (passport) {

  router.get('/', ensureAPIUser('any'), controller.index);
  router.get('/:id', ensureAPIUser(['admin']), controller.show);
  router.post('/', ensureAPIUser(['admin', 'teacher']), controller.create);
  router.put('/:id', ensureAPIUser(['admin', 'teacher']), controller.update);
  router.patch('/:id', ensureAPIUser(['admin', 'teacher']), controller.update);
  router['delete']('/:id', ensureAPIUser(['admin', 'teacher']), controller.destroy);
  return router;
};

module.exports = exports['default'];
//# sourceMappingURL=index.js.map
