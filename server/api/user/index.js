/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _componentsEnsureApiUser = require('../../components/ensureApiUser');

var _componentsEnsureApiUser2 = _interopRequireDefault(_componentsEnsureApiUser);

var express = require('express');
var controller = require('./user.controller');

var router = express.Router();
var ensureAPIUser = _componentsEnsureApiUser2['default'].ensureAPIUser;

exports['default'] = function (passport) {

  router.get('/', ensureAPIUser(['teacher', 'admin']), controller.index);
  router.get('/students', ensureAPIUser(['teacher', 'admin']), controller.getStudents);
  router.get('/teachers', ensureAPIUser(['admin']), controller.getTeachers);
  router.get('/logout', ensureAPIUser('any'), controller.logout);
  router.get('/current', ensureAPIUser('any'), controller.current);
  router.post('/forgot', controller.forgot);
  router.get('/:id', ensureAPIUser('any'), controller.show);
  router.post('/', ensureAPIUser(['teacher', 'admin']), controller.create);
  router.post('/register', controller.register);
  router.post('/createStudents', ensureAPIUser(['teacher', 'admin']), controller.createStudents);
  router.post('/:id/resetCode', ensureAPIUser(['teacher', 'admin']), controller.resetCode);
  router.post('/verify/:code', controller.verifyAccount);
  router.post('/:id/setTopicLevel', ensureAPIUser('any'), controller.setTopicLevel);
  router.post('/:id/mergeTo', ensureAPIUser(['admin']), controller.mergeStudent);
  router.post('/loginAs', ensureAPIUser(['admin']), controller.loginAs);
  router.post('/login', passport.authenticate('local'), controller.login);
  router.put('/:id', ensureAPIUser('any'), controller.update);
  router.patch('/:id', ensureAPIUser('any'), controller.update);
  router['delete']('/:id', ensureAPIUser(['admin']), controller.destroy);
  return router;
};

module.exports = exports['default'];
//# sourceMappingURL=index.js.map
