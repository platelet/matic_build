/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _shaJs = require('sha.js');

var _shaJs2 = _interopRequireDefault(_shaJs);

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

var UserSchema = new mongoose.Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  _hash: { type: String },
  _salt: { type: String },
  email: { type: String, unique: true },
  info: {
    teacher: { type: ObjectId, ref: 'User' },
    NSN: String,
    gender: String,
    ethnicity: String,
    yearLevel: Number,
    DOB: Number
  },
  returnTo: {
    stateName: { type: String },
    stateParams: Mixed
  },
  code: { type: String },
  dashboardEnabled: { type: Boolean, 'default': true },
  settings: { type: Object, 'default': {} },
  userType: { type: ObjectId, ref: 'UserType' },
  classroom: { type: ObjectId, ref: 'Classroom' },
  teacher: { type: ObjectId, ref: 'User' },
  stripeCustomer: { type: String, 'default': '' },
  verificationCode: { type: String, 'default': '' },
  resetCode: { type: String, 'default': '' },
  userWelcomed: { type: Boolean, 'default': false },
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Document Level Methods
 */
UserSchema.methods.verifyPassword = function (password) {
  var hashed_pass = (0, _shaJs2['default'])('sha256').update(String(password.toLowerCase()) + this._salt, 'utf-8').digest('hex');
  console.log(hashed_pass);
  if (this._hash === hashed_pass) return true;
  return false;
};

UserSchema.virtual('fullname').get(function () {
  return this.firstName + ' ' + this.lastName;
});

UserSchema.post('save', function (doc, next) {
  doc.populate('userType classroom teacher').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('User', UserSchema);
module.exports = exports['default'];
//# sourceMappingURL=user.model.js.map
