/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/users              ->  index
 * POST    /api/users              ->  create
 * GET     /api/users/:id          ->  show
 * PUT     /api/users/:id          ->  update
 * DELETE  /api/users/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.getStudents = getStudents;
exports.getTeachers = getTeachers;
exports.loginAs = loginAs;
exports.login = login;
exports.logout = logout;
exports.current = current;
exports.show = show;
exports.create = create;
exports.mergeStudent = mergeStudent;
exports.register = register;
exports.createStudents = createStudents;
exports.forgot = forgot;
exports.resetCode = resetCode;
exports.setTopicLevel = setTopicLevel;
exports.update = update;
exports.destroy = destroy;
exports.verifyAccount = verifyAccount;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _userModel = require('./user.model');

var _userModel2 = _interopRequireDefault(_userModel);

var _userTypeUserTypeModel = require('../userType/userType.model');

var _userTypeUserTypeModel2 = _interopRequireDefault(_userTypeUserTypeModel);

var _classroomClassroomModel = require('../classroom/classroom.model');

var _classroomClassroomModel2 = _interopRequireDefault(_classroomClassroomModel);

var _classroomAssignmentClassroomAssignmentModel = require('../classroomAssignment/classroomAssignment.model');

var _classroomAssignmentClassroomAssignmentModel2 = _interopRequireDefault(_classroomAssignmentClassroomAssignmentModel);

var _utils = require('../../utils');

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _shaJs = require('sha.js');

var _shaJs2 = _interopRequireDefault(_shaJs);

var _mail = require('../../mail');

var ObjectId = _mongoose2['default'].Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      delete entity._hash;
      delete entity._salt;
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function ensureCanModify(_id, res) {
  return function (entity) {
    if (String(entity.info.teacher) === String(_id)) return entity;
    res.status(401).end();
    return null;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Users

function index(req, res) {
  var search = {};
  if (req.user.userType.title === 'teacher') search = { 'teacher': req.user._id };

  _userModel2['default'].find(search).select('-_salt -_hash').populate('userType teacher classroom').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

function getStudents(req, res) {
  _userModel2['default'].find({ 'teacher': req.user._id }).select('-_salt -_hash').populate('userType info').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

function getTeachers(req, res) {
  _userTypeUserTypeModel2['default'].findOne({ title: 'teacher' }, function (err, obj) {
    _userModel2['default'].find({ userType: obj._id }).select('-_salt -_hash').populate('userType').execAsync().then(respondWithResult(res))['catch'](handleError(res));
  });
}

function loginAs(req, res) {
  _userModel2['default'].findOne({ _id: req.body.userId }, function (err, obj) {
    if (obj && obj._id) {
      req.login(obj, function (err) {
        if (err) console.log(err);else respondWithResult(res)(true);
      });
    }
  });
}

function login(req, res) {
  _userModel2['default'].findOne({ _id: req.user.id }, '-_salt -_hash').populate('userType', 'title permissionEnum redirectTo').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

function logout(req, res) {
  req.logout();
  res.redirect('/');
}

function current(req, res) {
  if (req.user) {
    res.json({ id: req.user._id, user: req.user });
  } else {
    res.json(false);
  }
}

// Gets a single User from the DB

function show(req, res) {
  _userModel2['default'].findById(req.params.id).select('-_salt -_hash').populate('userType teacher classroom').execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

function saltHash(object, subject) {
  var salt = (0, _utils.salter)();

  object._salt = salt;
  var hash = (0, _shaJs2['default'])('sha256').update(String(subject) + salt, 'utf-8').digest('hex');
  object._hash = hash;

  return object;
}

// Creates a new User in the DB

function create(req, res) {
  var userType = typeof req.body.userType === 'object' ? req.body.userType.title : 'teacher';

  if (req.user.userType.title === 'teacher') {
    _userTypeUserTypeModel2['default'].findOne({ title: 'student' }, function (err, userType) {
      _classroomClassroomModel2['default'].findOne({ teacher: req.user._id }, function (err, classroom) {
        req.body = saltHash(req.body, req.body.firstName.toLowerCase());
        req.body.code = (0, _utils.studentCode)();

        req.body.userType = userType;
        if (req.body.info) {
          req.body.info.teacher = req.user;
        } else {
          req.body.info = { teacher: req.user };
        }

        _userModel2['default'].createAsync(req.body).then(function (student) {
          _classroomClassroomModel2['default'].updateOne({ _id: classroom._id }, { $push: { students: student } }, function (err, res) {});
          return student;
        }).then(respondWithResult(res, 201))['catch'](handleError(res));
      });
    });
  } else {
    if (userType !== 'student') {
      req.body = saltHash(req.body, req.body.password);
      delete req.body.password;
      delete req.body.type;
      _userModel2['default'].find({ email: req.body.email }).exec().then(function (users) {
        if (users.length === 0) {
          if (req.user && req.user.userType.title === 'admin' && req.body.userType) {
            req.body.userType = typeof req.body.userType === "object" ? ObjectId(req.body.userType._id) : ObjectId(req.body.userType);

            _userModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
          } else {

            //new teacher
            _userTypeUserTypeModel2['default'].findOne({ title: 'teacher' }, function (e, uT) {
              req.body.userType = uT._id;
              req.body.dashboardEnabled = false;
              req.body.verificationCode = _crypto2['default'].randomBytes(4).toString('hex');
              _userModel2['default'].createAsync(req.body).then(function (user) {
                (0, _mail.sendVerifyEmail)(user.email, user.fullname, user.verificationCode);
                return user;
              }).then(respondWithResult(res, 201))['catch'](handleError(res));
            });
          }
        } else {
          handleError(res, 403)('User already exsists');
        }
      });
    } else {

      req.body.info = req.body.info ? _lodash2['default'].extend(req.body.info, { levels: [] }) : { levels: [] };
      delete req.body.type;
      req.body = saltHash(req.body, req.body.firstName.toLowerCase());
      req.body.code = (0, _utils.studentCode)();

      req.body.userType = typeof req.body.userType === "object" ? ObjectId(req.body.userType._id) : req.body.userType = ObjectId(req.body.userType);

      _userModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
    }
  }
}

function mergeStudent(req, res) {}

//Teacher Registration

function register(req, res) {
  req.body = saltHash(req.body, req.body.password);
  delete req.body.password;
  delete req.body.type;
  _userModel2['default'].find({ email: req.body.email }).exec().then(function (users) {
    if (users.length === 0) {
      _userTypeUserTypeModel2['default'].findOne({ title: 'teacher' }, function (e, uT) {
        req.body.userType = uT._id;
        req.body.dashboardEnabled = false;
        req.body.verificationCode = _crypto2['default'].randomBytes(4).toString('hex');
        _userModel2['default'].createAsync(req.body).then(function (user) {
          (0, _mail.sendVerifyEmail)(user.email, user.fullname, user.verificationCode);
          return user;
        }).then(function (user) {
          _classroomClassroomModel2['default'].createAsync({
            teacher: user,
            classroomName: user.fullname + "'s classroom"
          }).then(function (classroom) {
            var newAssignments = [{ classroom: ObjectId(classroom._id), assignmentType: 'TOPIC', linkedTopic: ObjectId("5bbe74c250392d0019ef0a8b") }, { classroom: ObjectId(classroom._id), assignmentType: 'ASSESSMENT', linkedAssessment: ObjectId("5cf4df3cbfb689001abadbc2") }, { classroom: ObjectId(classroom._id), assignmentType: 'SURVEY', linkedSurvey: ObjectId("5a0f47d252f2f800190b81c0") }];
            _classroomAssignmentClassroomAssignmentModel2['default'].insertMany(newAssignments, function (err, docs) {}).then(respondWithResult(res, 201))['catch'](handleError(res));
          })['catch'](handleError(res));
        })['catch'](handleError(res));
      });
    } else {
      handleError(res, 403)('User already exsists');
    }
  });
}

function createStudents(req, res) {
  _userTypeUserTypeModel2['default'].findOne({ title: 'student' }, function (e, t) {
    var objects = req.body.map(function (e, i) {
      var name = e.split(' ');
      var salt = (0, _utils.salter)();
      return {
        firstName: name[0].trim(),
        lastName: name[1].trim(),
        userType: ObjectId(t._id),
        info: {
          levels: []
        },
        teacher: ObjectId(req.user._id),
        _salt: salt,
        _hash: (0, _shaJs2['default'])('sha256').update(String(name[0].trim()).toLowerCase() + salt, 'utf-8').digest('hex'),
        code: (0, _utils.studentCode)()
      };
    });
    _userModel2['default'].collection.insertAsync(objects).then(respondWithResult(res, 201))['catch'](handleError(res));
  });
}

function forgot(req, res) {
  if (req.body.email) {
    _userModel2['default'].findOne({ email: req.body.email }).exec().then(function (user) {
      if (user) {
        var resetCode = _crypto2['default'].randomBytes(4).toString('hex');
        saveUpdates({ resetCode: resetCode })(user);
        (0, _mail.sendForgotEmail)(user.email, user.fullname, resetCode);
        respondWithResult(res)(true);
      } else {
        handleError(res, 204)('no user');
      }
    });
  } else if (req.body.resetCode) {
    _userModel2['default'].findOne({ resetCode: req.body.resetCode }).exec().then(function (user) {
      if (user) {
        var salt = (0, _utils.salter)();
        var _hash = (0, _shaJs2['default'])('sha256').update(String(req.body.password) + salt, 'utf-8').digest('hex');
        saveUpdates({ _hash: _hash, _salt: salt })(user);
        respondWithResult(res)(true);
      } else {
        handleError(res, 204)('no user');
      }
    });
  }
}

function resetCode(req, res) {
  if (req.user.userType.title === 'teacher') {
    _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(ensureCanModify(req.user._id, res)).then(saveUpdates({ code: (0, _utils.studentCode)() })).then(respondWithResult(res))['catch'](handleError(res));
  } else if (req.user.userType.title === 'admin') {
    _userModel2['default'].findById(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates({ code: (0, _utils.studentCode)() })).then(respondWithResult(res))['catch'](handleError(res));
  }
}

function setTopicLevel(req, res) {
  if (req.user._id.toString() !== req.params.id.toString()) {
    handleError(res, 403)('Permission Denied');
  } else {
    var info = {};
    _userModel2['default'].findById(req.params.id).then(handleEntityNotFound(res)).then(function (user) {
      info = user.info;
      if (info.topics) {
        info.topics[req.body.topicId] = {
          currentLevel: parseInt(req.body.level),
          unlockedLevels: Array.apply(null, { length: parseInt(req.body.level) + 2 }).map(function (e, i) {
            return i + 1;
          })
        };
      } else {
        info.topics = {};
        info.topics[req.body.topicId] = {
          currentLevel: parseInt(req.body.level),
          unlockedLevels: Array.apply(null, { length: parseInt(req.body.level) + 2 }).map(function (e, i) {
            return i + 1;
          })
        };
      }
      return saveUpdates({ info: info })(user);
    }).then(respondWithResult(res))['catch'](handleError(res));
  }
}

// Updates an existing User in the DB

function update(req, res) {
  if (req.body._hash) delete req.body._hash;
  if (req.body._salt) delete req.body._salt;

  if (req.body.password && req.body.password.length > 3) {
    var salt = (0, _utils.salter)();
    var _hash = (0, _shaJs2['default'])('sha256').update(String(req.body.password) + salt, 'utf-8').digest('hex');
    req.body._hash = _hash;
    req.body._salt = salt;
  }

  if (req.user.userType.title === 'teacher') {
    if (String(req.user._id) === String(req.body._id)) {
      _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
    } else {
      _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
    }
  } else if (req.user.userType.title !== 'admin') {
    if (req.user._id.toString() !== req.body._id.toString()) {
      handleError(res, 401)('permission denied');
      return;
    }
    if (req.body.dashboardEnabled) delete req.body.dashboardEnabled;

    if (req.body._id) delete req.body._id;

    _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
  } else {
    if (req.body._id) delete req.body._id;

    _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
  }
}

// Deletes a User from the DB

function destroy(req, res) {
  if (req.user.userType.title === 'admin') {
    _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
  } else if (req.user.userType.title === 'teacher') {
    _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(ensureCanModify(req.user._id, res)).then(removeEntity(res))['catch'](handleError(res));
  }
}

function verifyAccount(req, res) {
  var code = req.params.code;

  if (code.length !== 8) return res.status(204).end();

  _userModel2['default'].findOne({ verificationCode: code }).exec().then(function (user) {
    if (!user) return res.status(204).end();

    user.dashboardEnabled = true;
    user.verificationCode = '';
    user.save().then(respondWithResult(res))['catch'](handleError(res));
  });
}
//# sourceMappingURL=user.controller.js.map
