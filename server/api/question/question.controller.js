/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/questions         ->  index
 * POST    /api/questions         ->  create
 * GET     /api/questions/:id     ->  show
 * PUT     /api/questions/:id     ->  update
 * DELETE  /api/questions/:id     ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.fetchBulk = fetchBulk;
exports.show = show;
exports.showObjectiveQuestions = showObjectiveQuestions;
exports.showTest = showTest;
exports.create = create;
exports.bulk = bulk;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _questionModel = require('./question.model');

var _questionModel2 = _interopRequireDefault(_questionModel);

var _questionMetricQuestionMetricModel = require('../questionMetric/questionMetric.model');

var _questionMetricQuestionMetricModel2 = _interopRequireDefault(_questionMetricQuestionMetricModel);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Questions

function index(req, res) {
  _questionModel2['default'].find().populate('image objective').sort('questionNumber').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

function fetchBulk(req, res) {
  _questionModel2['default'].find({ _id: { $in: req.body } }).populate('image objective').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single Question from the DB

function show(req, res) {
  _questionModel2['default'].findById(req.params.id).populate('image').execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

function showObjectiveQuestions(req, res) {
  _questionModel2['default'].find({ "objective": req.params.objectiveId }).populate('image objective').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

function showTest(req, res) {
  _questionModel2['default'].find({ "objective": req.params.objectiveId }).populate('image objective').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Question in the DB

function create(req, res) {
  req.body.objective = ObjectId(req.body.objective._id);
  if (req.body.image === false) {
    delete req.body.image;
  } else {
    req.body.image = ObjectId(req.body.image._id);
  }
  _questionModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Create new questions from uploaded .CSV file

function bulk(req, res) {
  var toInsert = [];
  var toUpdate = [];
  var _q = req.body.map(function (q, i) {
    q.objective = ObjectId(q.objective);
    if (q.image) q.image = ObjectId(q.image);
    q.createdAt = Date.now();
    q.updatedAt = Date.now();
    if (q._id && q._id.length === 24) {
      q._id = ObjectId(q._id);
      toUpdate.push(q);
    } else {
      toInsert.push(q);
    }
    return q;
  });
  if (toUpdate.length > 0) {
    toUpdate.map(function (q, i) {
      _questionModel2['default'].findByIdAsync(q._id).then(handleEntityNotFound(res)).then(saveUpdates(q));
    });
  }
  if (toInsert.length > 0) {
    _questionModel2['default'].collection.insertAsync(toInsert).then(respondWithResult(res, 201))['catch'](handleError(res));
  } else {
    respondWithResult(res, 201)({});
  }
}

// Updates an existing Question in the DB

function update(req, res) {
  if (req.body._id) delete req.body._id;
  if (req.body.$loki) delete req.body.$loki;
  if (req.body.meta) delete req.body.meta;

  req.body.objective = ObjectId(req.body.objective._id);
  if (req.body.image) req.body.image = ObjectId(req.body.image._id);
  _questionModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Question from the DB

function destroy(req, res) {
  _questionMetricQuestionMetricModel2['default'].remove({ question: ObjectId(req.params.id) }).execAsync().then(function (response) {
    console.log(response);
  });
  _questionModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=question.controller.js.map
