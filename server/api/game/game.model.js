/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

var GameSchema = new mongoose.Schema({
  name: { type: String, 'default': '' },
  icon: { type: String },
  color: { type: String, 'default': 'green' },
  gameIdentifier: { type: String, 'default': '' },
  enabled: { type: Boolean, 'default': true }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Document Level Methods
 */

exports['default'] = mongoose.model('Game', GameSchema);
module.exports = exports['default'];
//# sourceMappingURL=game.model.js.map
