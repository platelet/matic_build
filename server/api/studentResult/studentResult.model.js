/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var StudentResultSchema = new mongoose.Schema({
  objective: { type: ObjectId, ref: 'Objective' },
  activity: { type: ObjectId },
  assignment: { type: ObjectId, ref: 'ClassroomAssignment' },
  student: { type: ObjectId, ref: 'User' },
  results: {
    correct: Number,
    incorrect: Number,
    grade: { type: String, 'enum': ['gold', 'silver', 'bronze', 'na'] }
  },
  resultType: { type: String, emum: ['questions', 'game', 'assessment_questions', 'assessment_game'] },
  assessmentType: { type: String, emum: ['A', 'B'] },
  questionMeta: {
    questions: Object,
    questionIds: Array
  },
  gameMeta: {},
  elapsedTime: Number,
  completedAt: { type: Number, index: true }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

StudentResultSchema.virtual('gradePoint').get(function () {
  switch (this.results.grade) {
    case "gold":
      return 4;
      break;
    case "silver":
      return 3;
      break;
    case "bronze":
      return 2;
      break;
    default:
      return 1;
  }
});

StudentResultSchema.post('save', function (doc, next) {
  doc.populate('student objective').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('StudentResult', StudentResultSchema);
module.exports = exports['default'];
//# sourceMappingURL=studentResult.model.js.map
