/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/testResults              ->  index
 * POST    /api/testResults              ->  create
 * GET     /api/testResults/:id          ->  show
 * PUT     /api/testResults/:id          ->  update
 * DELETE  /api/testResults/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.bulkGet = bulkGet;
exports.adminAssessmentQuery = adminAssessmentQuery;
exports.objectiveTaskQuery = objectiveTaskQuery;
exports.adminQuery = adminQuery;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _studentAssessmentResultStudentAssessmentResultModel = require('../studentAssessmentResult/studentAssessmentResult.model');

var _studentAssessmentResultStudentAssessmentResultModel2 = _interopRequireDefault(_studentAssessmentResultStudentAssessmentResultModel);

var _studentResultModel = require('./studentResult.model');

var _studentResultModel2 = _interopRequireDefault(_studentResultModel);

var _questionMetricQuestionMetricModel = require('../questionMetric/questionMetric.model');

var _questionMetricQuestionMetricModel2 = _interopRequireDefault(_questionMetricQuestionMetricModel);

var _classroomClassroomModel = require('../classroom/classroom.model');

var _classroomClassroomModel2 = _interopRequireDefault(_classroomClassroomModel);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Types.ObjectId;

function filterForUser(user) {
  user = user;
  return function (docs) {
    if (user.userType.title === 'admin' || user.userType.title === 'student') {
      return docs;
    }

    if (typeof docs === 'object' && docs.length) {
      docs = docs.filter(function (e, i) {
        if (e.student && e.student.info) return String(e.student.info.teacher) === String(user._id);else return false;
      });
      return docs;
    } else {
      if (docs.student.info.teacher === user._id) return docs;
    }
  };
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of StudentResults

function index(req, res) {
  if (req.user.userType.title === 'student') {
    var params = { student: req.user._id };
    _studentResultModel2['default'].find(params).populate('student objective').sort('-completedAt').execAsync().then(respondWithResult(res))['catch'](handleError(res));
  } else if (req.user.userType.title === 'teacher') {
    _classroomClassroomModel2['default'].find({ teacher: req.user._id }).exec().then(function (classrooms) {
      var ids = [];
      classrooms.forEach(function (classroom) {
        ids = ids.concat(classroom.students);
      });
      _studentResultModel2['default'].find({ student: { $in: ids } }).populate('student objective').sort('-completedAt').exec().then(respondWithResult(res))['catch'](handleError(res));
    })['catch'](handleError(res));
  } else {
    var params = {};
    _studentResultModel2['default'].find(params).populate('student objective').sort('-completedAt').batchSize(100000).execAsync().then(filterForUser(req.user)).then(respondWithResult(res))['catch'](handleError(res));
  }
}

function bulkGet(req, res) {
  var students = req.body.students;
  var objectives = req.body.objectives;

  if (typeof students === 'object' && students.length && students.length !== 0 && typeof objectives === 'object' && objectives.length && objectives.length !== 0) {
    _studentResultModel2['default'].find({
      objective: { $in: objectives },
      student: { $in: students }
    }).execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
  } else {
    respondWithResult(res)([]);
  }
}

function adminAssessmentQuery(req, res) {
  // req.body : {
  //   teacher: this.$scope.selectedTeacher._id,
  //   dateStart: new moment(this.exportSettings.dateStart).hour(0).minute(0).second(0).unix(),
  //   dateEnd: new moment(this.exportSettings.dateEnd).hour(23).minute(59).second(59).unix(),
  //   activity: this.$scope.selectedAssessment._id
  // }
  var query = {
    assessment: ObjectId(req.body.activity),
    completedAt: { $lt: req.body.dateEnd, $gt: req.body.dateStart }
  };
  _studentAssessmentResultStudentAssessmentResultModel2['default'].find(query).populate('student linkedResults').sort('-completedAt').batchSize(100000).execAsync().then(function (response) {
    //filter for teacher
    if (req.body.teacher !== 'all') {
      response = response.filter(function (result) {
        if (result.student && result.student.info) {
          return String(result.student.info.teacher) === String(req.body.teacher);
        }
        return false;
      });
    }
    return response;
  }).then(function (response) {
    var studentResults = [];
    response.forEach(function (result) {
      if (result && result.linkedResults && result.linkedResults[0]) {
        result.linkedResults[0].assessmentType = "A";
        studentResults.push(result.linkedResults[0]);
      }
      if (result.linkedResults && result.linkedResults[1] && result.linkedResults[1].resultType === "assessment_questions") {
        result.linkedResults[1].assessmentType = "B";
        studentResults.push(result.linkedResults[1]);
      }
    });
    return studentResults;
  }).then(respondWithResult(res))['catch'](handleError(res));
}

function objectiveTaskQuery(req, res) {
  _studentResultModel2['default'].find({
    student: { $in: req.body.students },
    objective: { $in: req.body.objectives },
    resultType: { $in: ['questions', 'game'] }
  }).then(respondWithResult(res))['catch'](handleError(res));
}

//Get range of results for admin audit

function adminQuery(req, res) {
  // req.body : {
  //   objective: ...,
  //   dateStart: ...,
  //   dateEnd: ...,
  //   activity: ...,
  //   activityType: ...
  // }
  var query = {
    objective: ObjectId(req.body.objective),
    completedAt: { $lt: req.body.dateEnd, $gt: req.body.dateStart }
  };
  if (req.body.activityType === 'QUESTIONS_GAME') {
    query.resultType = 'questions';
  } else {
    query.resultType = 'game';
    query.activity = ObjectId(req.body.activity);
  }

  _studentResultModel2['default'].find(query).populate('student objective').sort('-completedAt').batchSize(100000).execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single StudentResult from the DB

function show(req, res) {
  if (req.user.userType.title === 'student') {
    var params = { _id: req.params.id, student: req.user._id };
  } else {
    var params = { _id: req.params.id };
  }
  _studentResultModel2['default'].find(params).limit(1).populate('student objective').execAsync().then(handleEntityNotFound(res)).then(filterForUser(req.user)).then(respondWithResult(res))['catch'](handleError(res));
}

function saveMetric(question) {
  _questionMetricQuestionMetricModel2['default'].findOne({ question: question._id, answer: question.answer }, function (err, obj) {
    if (obj) {
      obj.count += 1;
      obj.save();
    } else {
      _questionMetricQuestionMetricModel2['default'].createAsync({
        question: question._id,
        answer: question.answer,
        count: 1
      });
    }
  });
}

// Creates a new StudentResult in the DB

function create(req, res) {
  if (req.user.userType.title === 'student') {
    // for (let key of Object.keys(req.body.questions)) {
    //   delete req.body.questions[key].question;//save some space in the db
    //   var question = req.body.questions[key];
    //   saveMetric(question);
    // }
    req.body.student = ObjectId(req.user._id);
    if (req.body.objective) req.body.objective = ObjectId(req.body.objective._id);
    _studentResultModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
  } else {
    respondWithResult(res, 401)(false);
  }
}

// Updates an existing StudentResult in the DB

function update(req, res) {
  if (req.body._id) delete req.body._id;
  if (req.body.$loki) delete req.body.$loki;
  if (req.body.meta) delete req.body.meta;

  _studentResultModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a StudentResult from the DB

function destroy(req, res) {
  _studentResultModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=studentResult.controller.js.map
