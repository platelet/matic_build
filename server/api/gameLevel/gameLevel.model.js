/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

var GameLevelSchema = new mongoose.Schema({
  game: { type: ObjectId, ref: 'Game' },
  name: { type: String, 'default': '' },
  rounds: { type: Number, 'default': 10 },
  displayName: { type: String },
  grading: {
    gold: { type: Number, 'default': 30 },
    silver: { type: Number, 'default': 60 }
  },
  settings: { type: Object, 'default': {} }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Document Level Methods
 */

exports['default'] = mongoose.model('GameLevel', GameLevelSchema);
module.exports = exports['default'];
//# sourceMappingURL=gameLevel.model.js.map
