'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Schema.ObjectId;

var SurveyResponseSchema = new _mongoose2['default'].Schema({
  survey: { type: ObjectId, ref: 'Survey' },
  assignment: { type: ObjectId, ref: 'ClassroomAssignment' },
  user: { type: ObjectId, ref: 'User' },
  responses: { type: Array },
  completedAt: { type: Date, 'default': Date.now }
});

exports['default'] = _mongoose2['default'].model('SurveyResponse', SurveyResponseSchema);
module.exports = exports['default'];
//# sourceMappingURL=surveyResponse.model.js.map
