/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2018                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/surveyResponses              ->  index
 * POST    /api/surveyResponses              ->  create
 * GET     /api/surveyResponses/:id          ->  show
 * PUT     /api/surveyResponses/:id          ->  update
 * DELETE  /api/surveyResponses/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _surveyResponseModel = require('./surveyResponse.model');

var _surveyResponseModel2 = _interopRequireDefault(_surveyResponseModel);

var _classroomClassroomModel = require('../classroom/classroom.model');

var _classroomClassroomModel2 = _interopRequireDefault(_classroomClassroomModel);

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of SurveyResponses

function index(req, res) {
  if (req.user.userType.title === 'student') {
    var params = { user: req.user._id };
    _surveyResponseModel2['default'].find(params).populate('user survey').sort('-completedAt').execAsync().then(respondWithResult(res))['catch'](handleError(res));
  } else if (req.user.userType.title === 'teacher') {
    _classroomClassroomModel2['default'].find({ teacher: req.user._id }).exec().then(function (classrooms) {
      var ids = [];
      classrooms.forEach(function (group) {
        ids = ids.concat(group.students);
      });
      _surveyResponseModel2['default'].find({ user: { $in: ids } }).populate('user survey').sort('-completedAt').exec().then(respondWithResult(res))['catch'](handleError(res));
    })['catch'](handleError(res));
  } else {
    var params = {};
    StudentAssessmentResult.find(params).populate('user survey').sort('-completedAt').batchSize(100000).execAsync().then(filterForUser(req.user)).then(respondWithResult(res))['catch'](handleError(res));
  }
}

// Gets a single SurveyResponse from the DB

function show(req, res) {
  return _surveyResponseModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new SurveyResponse in the DB

function create(req, res) {
  req.body.user = req.user;
  return _surveyResponseModel2['default'].create(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Updates an existing SurveyResponse in the DB

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _surveyResponseModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a SurveyResponse from the DB

function destroy(req, res) {
  return _surveyResponseModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=surveyResponse.controller.js.map
