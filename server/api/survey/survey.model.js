/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

var SurveySchema = new mongoose.Schema({
  name: String,
  enabled: Boolean,
  icon: String,
  color: String,
  imageName: String,
  userType: {
    type: ObjectId,
    ref: 'UserType'
  },
  questions: Array,
  description: { type: String, 'default': '' },
  teacherDescription: { type: String, 'default': '' },
  studentDescription: { type: String, 'default': '' }
});

exports['default'] = mongoose.model('Survey', SurveySchema);
module.exports = exports['default'];
//# sourceMappingURL=survey.model.js.map
