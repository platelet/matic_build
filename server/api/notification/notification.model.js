/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

var NotificationSchema = new mongoose.Schema({
  icon: String,
  title: String,
  message: String,
  broadcastTo: [{
    type: ObjectId,
    ref: 'UserType'
  }],
  readBy: [{ type: ObjectId }],
  broadcastStart: { type: Date, 'default': Date.now },
  broadcastEnd: { type: Date },
  createdAt: { type: Date, 'default': Date.now }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Document Level Methods
 */

exports['default'] = mongoose.model('Notification', NotificationSchema);
module.exports = exports['default'];
//# sourceMappingURL=notification.model.js.map
