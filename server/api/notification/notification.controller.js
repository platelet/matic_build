/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/notifications              ->  index
 * POST    /api/notifications              ->  create
 * GET     /api/notifications/:id          ->  show
 * PUT     /api/notifications/:id          ->  update
 * DELETE  /api/notifications/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _notificationModel = require('./notification.model');

var _notificationModel2 = _interopRequireDefault(_notificationModel);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      delete entity._hash;
      delete entity._salt;
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function ensureCanModify(_id, res) {
  return function (entity) {
    if (String(entity.info.teacher) === String(_id)) return entity;
    res.status(401).end();
    return null;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Notifications

function index(req, res) {
  if (req.user.userType.title === 'admin') {
    _notificationModel2['default'].find({}).populate('broadcastTo').execAsync().then(respondWithResult(res))['catch'](handleError(res));
  } else {
    var search = {
      broadcastTo: req.user.userType._id,
      readBy: { $ne: req.user._id },
      broadcastStart: { $lt: Date.now() },
      broadcastEnd: { $gt: Date.now() }
    };

    _notificationModel2['default'].find(search).populate('broadcastTo').execAsync().then(respondWithResult(res))['catch'](handleError(res));
  }
}

// Gets a single Notification from the DB

function show(req, res) {
  _notificationModel2['default'].findById(req.params.id).execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new _Notification in the DB

function create(req, res) {
  _notificationModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Updates an existing _Notification in the DB

function update(req, res) {
  if (req.body.$loki) delete req.body.$loki;
  if (req.body.meta) delete req.body.meta;

  _notificationModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a _Notification from the DB

function destroy(req, res) {
  _notificationModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=notification.controller.js.map
