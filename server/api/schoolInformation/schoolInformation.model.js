/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var SchoolInformationSchema = new mongoose.Schema({
  schoolNumber: Number,
  schoolName: String,
  principal: String,
  contact: {
    telephone: String,
    fax: String,
    email: String,
    website: String
  },
  address: {
    street: String,
    suburb: String,
    townCity: String,
    postalAddress_1: String,
    postalAddress_2: String,
    postalAddress_3: String,
    postalCode: String,
    urbanArea: String,
    latlng: {
      longitude: Number,
      latitude: Number
    }
  },
  schoolType: String,
  definition: String,
  schoolGender: String,
  communityOfLearning: {
    id: Number,
    name: String
  },
  schoolRoll: {
    total: { type: Number, 'default': 0 },
    europeanPakeha: { type: Number, 'default': 0 },
    maori: { type: Number, 'default': 0 },
    pacific: { type: Number, 'default': 0 },
    asian: { type: Number, 'default': 0 },
    MELAA: { type: Number, 'default': 0 },
    other: { type: Number, 'default': 0 },
    international: { type: Number, 'default': 0 }
  },
  decile: { type: Number, 'default': 1 },
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

exports['default'] = mongoose.model('SchoolInformation', SchoolInformationSchema);
module.exports = exports['default'];
//# sourceMappingURL=schoolInformation.model.js.map
