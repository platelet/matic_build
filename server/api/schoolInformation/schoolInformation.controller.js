/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2019 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2019                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.search = search;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _schoolInformationModel = require('./schoolInformation.model');

var _schoolInformationModel2 = _interopRequireDefault(_schoolInformationModel);

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Subjects

function index(req, res) {
  _schoolInformationModel2['default'].find().execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

function show(req, res) {
  _schoolInformationModel2['default'].findById(req.params.id).execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

function search(req, res) {
  var query = { schoolName: { '$regex': decodeURIComponent(req.params.term), '$options': 'i' } };
  _schoolInformationModel2['default'].find(query).select('_id schoolName address').sort('name').limit(50).execAsync().then(respondWithResult(res, 201))['catch'](handleError(res));
}

function create(req, res) {
  _schoolInformationModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

function update(req, res) {
  _schoolInformationModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

function destroy(req, res) {
  _schoolInformationModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=schoolInformation.controller.js.map
