/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _configEnvironment = require('../../config/environment');

var _configEnvironment2 = _interopRequireDefault(_configEnvironment);

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var ObjectiveSchema = new mongoose.Schema({
  topic: { type: ObjectId, ref: 'Topic' },
  subject: { type: ObjectId, ref: 'Subject' },
  index: { type: Number, 'default': 1 },
  objective: { type: String, 'default': '' },
  explainer: { type: String, 'default': '' },
  linkedActivities: [{ type: ObjectId, ref: 'Activity' }],
  linkedGames: [{ type: ObjectId, ref: 'GameLevel' }],
  linkedWordProblems: [{ type: ObjectId, ref: 'WordProblemSet' }],
  linkedSurveys: [{ type: ObjectId, ref: 'Survey' }],
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

ObjectiveSchema.virtual('level').get(function () {
  if (this.subject) {
    return this.subject.topicLevel;
  } else {
    return false;
  }
});

ObjectiveSchema.post('save', function (doc, next) {
  doc.populate('topic').populate('subject').populate('linkedActivities').populate({
    path: 'linkedGames',
    populate: {
      path: 'game',
      model: 'Game'
    }
  }).populate('linkedSurveys').populate('linkedWordProblems').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('Objective', ObjectiveSchema);
module.exports = exports['default'];
//# sourceMappingURL=objective.model.js.map
