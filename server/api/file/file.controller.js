/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/files                ->  index
 * POST    /api/files                ->  create
 * GET     /api/files/:id            ->  show
 * GET     /api/files/:id/download   ->  download
 * PUT     /api/files/:id            ->  update
 * DELETE  /api/files/:id            ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.download = download;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _fileModel = require('./file.model');

var _fileModel2 = _interopRequireDefault(_fileModel);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _s3 = require('s3');

var _s32 = _interopRequireDefault(_s3);

var _configAwsJs = require('../../config/aws.js');

var _configAwsJs2 = _interopRequireDefault(_configAwsJs);

var s3Client = _s32['default'].createClient({
  maxAsyncS3: 20,
  s3RetryCount: 3,
  s3RetryDelay: 1000,
  multipartUploadThreshold: 20971520,
  multipartUploadSize: 15728640,
  s3Options: {
    accessKeyId: "AKIAJW3755B3KI3KXEPA",
    secretAccessKey: "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"
  }
});

function userPerms(user, perms) {
  if (user && user.userType) {
    if (typeof perms === "string") {
      return user.userType.title === perms;
    } else if (typeof perms === "object" && perms.length) {
      return perms.indexOf(user.userType.title) !== -1;
    }
  }
  return false;
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removes3Asset(entity) {
  var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
  if (env === 'development') {
    var bucket = 'math-adv-matic-dev';
  } else {
    var bucket = 'math-adv-matic';
  }
  var params = {
    Bucket: bucket, /* required */
    Delete: {
      Objects: [{
        Key: entity.awsFolder + "/" + entity.awsBucketKey
      }]
    }
  };
  s3Client.deleteObjects(params, function (err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else console.log(data); // successful response
  });
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      removes3Asset(entity);
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Files

function index(req, res) {
  _fileModel2['default'].find().execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single File from the DB

function show(req, res) {
  _fileModel2['default'].findById(req.params.id).execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

//Initiates a download request

function download(req, res) {
  _fileModel2['default'].findById(req.params.id).execAsync().then(handleEntityNotFound(res)).then(function (file) {
    res.send('http://file-entity-here');
  })['catch'](handleError(res));
}

// Creates a new File in the DB

function create(req, res) {
  //define bucket from ENV
  var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
  if (env === 'development') {
    var bucket = "math-adv-matic-dev";
  } else {
    var bucket = "math-adv-matic";
  }

  var files = req.body.files.map(function (file, i) {
    var extension = file.detail.path.match(/\.[0-9a-z]+$/i)[0];
    return {
      name: file.name,
      awsBucket: bucket,
      awsFolder: "files",
      awsBucketKey: file.detail.base + extension
    };
  });

  var params = {
    localDir: _path2['default'].join(__dirname + '/../../tmp_files'),
    s3Params: {
      Prefix: "files",
      Bucket: bucket,
      ACL: 'public-read'
    }
  };

  var uploader = s3Client.uploadDir(params);
  //UPLOADER EVENT START
  uploader.on('progress', function () {
    console.log("progress", uploader.progressMd5Amount, uploader.progressAmount, uploader.progressTotal);
  });
  uploader.on('error', function (err) {
    console.error("unable to upload:", err.stack);
  });
  uploader.on('end', function () {
    try {
      var _tmp_files = _fs2['default'].readdirSync(_path2['default'].join(__dirname + '/../../tmp_files'));
    } catch (e) {
      return;
    }
    if (_tmp_files.length > 0) for (var i = 0; i < _tmp_files.length; i++) {
      var filePath = _path2['default'].join(__dirname + '/../../tmp_files/', _tmp_files[i]);
      if (_fs2['default'].statSync(filePath).isFile()) _fs2['default'].unlinkSync(filePath);
    }
    _fileModel2['default'].createAsync(files).then(respondWithResult(res, 201))['catch'](handleError(res));
  });
  //UPLOADER EVENTS END
}

// Updates an existing File in the DB

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  _fileModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a File from the DB

function destroy(req, res) {
  _fileModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=file.controller.js.map
