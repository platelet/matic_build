/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

var FileSchema = new mongoose.Schema({
  name: { type: String },
  awsBucket: { type: String },
  awsFolder: { type: String },
  awsBucketKey: { type: String },
  enabled: { type: Boolean, 'default': true },
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

FileSchema.virtual('url').get(function () {
  var baseUrl = 'https://s3-ap-southeast-2.amazonaws.com/';
  return baseUrl + this.awsBucket + '/' + this.awsFolder + '/' + encodeURIComponent(this.awsBucketKey);
});

exports['default'] = mongoose.model('File', FileSchema);
module.exports = exports['default'];
//# sourceMappingURL=file.model.js.map
