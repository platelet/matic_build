/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;

var SubjectSchema = new mongoose.Schema({
  topic: { type: ObjectId, ref: 'Topic' },
  topicLevel: { type: Number, 'default': 1 },
  description: { type: String, 'default': '' },
  teacherDescription: { type: String, 'default': '' },
  explainer: { type: String, 'default': '' },
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

SubjectSchema.post('save', function (doc, next) {
  doc.populate('topic').execPopulate().then(function () {
    next();
  });
});

exports['default'] = mongoose.model('Subject', SubjectSchema);
module.exports = exports['default'];
//# sourceMappingURL=subject.model.js.map
